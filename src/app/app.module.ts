import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from "@angular/forms";
import { AgmCoreModule } from "@agm/core";

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { SideMenuComponent } from './components/side-menu/side-menu.component';
import { LoginComponent } from './components/login/login.component';
import { PageNameComponent } from './components/page-name/page-name.component';
import { FooterComponent } from './components/footer/footer.component';
import { GoogleMapComponent } from './components/google-map/google-map.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';

import { FirmPageComponent } from './pages/firm-page/firm-page.component';
import { UnitPageComponent } from './pages/unit-page/unit-page.component';
import { JobTypePageComponent } from './pages/job-type-page/job-type-page.component';
import { EmployeePageComponent } from './pages/employee-page/employee-page.component';
import { CustomerPageComponent } from './pages/customer-page/customer-page.component';
import { RolePageComponent } from './pages/role-page/role-page.component';
import { BulkUploadPageComponent } from './pages/bulk-upload-page/bulk-upload-page.component';
import { CallPageComponent } from './pages/call-page/call-page.component';
import { ReminderPageComponent } from './pages/reminder-page/reminder-page.component';
import { AccessControlPageComponent } from './pages/access-control-page/access-control-page.component';
import { CardComponent } from './components/card/card.component';
import { CardHeaderComponent } from './components/card/card-header/card-header.component';
import { CardContentComponent } from './components/card/card-content/card-content.component';
import { CardFooterComponent } from './components/card/card-footer/card-footer.component';
import { PaginationComponent } from './components/pagination/pagination.component';
import { TableComponent } from './components/table/table.component';
import { CallBoxComponent } from './components/call-box/call-box.component';
import { FormComponent } from './components/form/form.component';
import { FirmAddPageComponent } from './pages/firm-add-page/firm-add-page.component';
import { CallAddPageComponent } from './pages/call-add-page/call-add-page.component';
import { CustomerAddPageComponent } from './pages/customer-add-page/customer-add-page.component';
import { EmployeeAddPageComponent } from './pages/employee-add-page/employee-add-page.component';
import { RoleAddPageComponent } from './pages/role-add-page/role-add-page.component';
import { UnitAddPageComponent } from './pages/unit-add-page/unit-add-page.component';
import { JobTypeAddPageComponent } from './pages/job-type-add-page/job-type-add-page.component';
import { ReminderAddPageComponent } from './pages/reminder-add-page/reminder-add-page.component';
import { HttpClientWrapper } from './HttpClientWrapper';
import { HttpClientModule } from '@angular/common/http';
@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    SideMenuComponent,
    LoginComponent,
    PageNameComponent,
    FooterComponent,
    GoogleMapComponent,
    DashboardComponent,
    FirmPageComponent,
    UnitPageComponent,
    JobTypePageComponent,
    EmployeePageComponent,
    CustomerPageComponent,
    RolePageComponent,
    BulkUploadPageComponent,
    CallPageComponent,
    ReminderPageComponent,
    AccessControlPageComponent,
    CardComponent,
    CardHeaderComponent,
    CardContentComponent,
    CardFooterComponent,
    PaginationComponent,
    TableComponent,
    CallBoxComponent,
    FormComponent,
    FirmAddPageComponent,
    CallAddPageComponent,
    CustomerAddPageComponent,
    EmployeeAddPageComponent,
    RoleAddPageComponent,
    UnitAddPageComponent,
    JobTypeAddPageComponent,
    ReminderAddPageComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule, 
    AgmCoreModule.forRoot({
      apiKey: "AIzaSyC6FEWBa-YmN64Xcw_yZ0T_0AqM2h6NaE8"
    }),
    HttpClientModule
  ],
  providers: [HttpClientWrapper],
  bootstrap: [AppComponent]
})
export class AppModule { }
