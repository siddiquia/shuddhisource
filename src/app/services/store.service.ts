import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Header } from '../components/header/header';
import { SideMenu } from '../components/side-menu/side-menu';
import { PageName } from '../components/page-name/page-name';
import { CallBox } from '../components/call-box/call-box';
import { Table } from '../components/table/table';

@Injectable({
  providedIn: 'root'
})
export class StoreService {

  public header = new BehaviorSubject<Header>(new Header());

  public sideMenu = new BehaviorSubject<SideMenu>(new SideMenu());

  public user = new BehaviorSubject<User>(new User());

  public pageName = new BehaviorSubject<PageName>(new PageName());

  public callBox = new BehaviorSubject<CallBox>(new CallBox());

  public firmTable = new BehaviorSubject<Table>(new Table());

  public unitTable = new BehaviorSubject<Table>(new Table());

  public jobTypeTable = new BehaviorSubject<Table>(new Table());

  public employeeTable = new BehaviorSubject<Table>(new Table());

  public roleTable = new BehaviorSubject<Table>(new Table());

  public customerTable = new BehaviorSubject<Table>(new Table());

  public callTable = new BehaviorSubject<Table>(new Table());

  public reminderTable = new BehaviorSubject<Table>(new Table());

  constructor() { }
}

export class User {
  public login: boolean;
  public name: string;
  public email: string;
  public status: boolean;

  constructor() {
    this.login = false;
    this.name = "";
    this.email = "";
    this.status = false;
  }

  public copy(user: User) {
    return Object.assign(this, user);
  }

}


