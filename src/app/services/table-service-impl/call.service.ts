import { Injectable } from '@angular/core';
import { TableService } from './table-service';
import { StoreService } from '../store.service';
import { Table } from 'src/app/components/table/table';
import { Call } from 'src/app/pages/call-page/call';
import { Column } from 'src/app/components/table/column';
import { HttpClientWrapper } from '../../HttpClientWrapper';
import { Uriconstant } from '../../uri-constant';
import { IfStmt } from '@angular/compiler';

@Injectable({
  providedIn: 'root'
})
export class CallService implements TableService {

  constructor(public store: StoreService, private httpClientWrapper: HttpClientWrapper) {
    this.getData();
  }

  getData() {
    //_currentPage=1&&_recordsPerPage=2&&_totalPages=
    let url = Uriconstant.END_POINT + Uriconstant.GET_CALL_LIST //+ "_currentPage=" + 1 + "&&_recordsPerPage=" + 10 + "&&_totalPages=";//"http://localhost/shuddhi/GetAllCallController/list?_currentPage=1&&_recordsPerPage=2&&_totalPages=";
    console.log("getList=====================>", getList);
    this.httpClientWrapper.post(url, getList).subscribe(response => {
      console.log("response is ====>", response, "<--------");
      let serviceResponse: any = response
      if (serviceResponse) {
        let table = new Table().copy(this.store.callTable.value);

        table.lazyLoad = serviceResponse.lazyLoad//fixture.lazyLoad;
        table.data = serviceResponse.payLoad.data; //fixture.data;

        if (table.lazyLoad) {
          table.pagination.totalRecords = serviceResponse.totalRecords;
        } else {
          table.pagination.totalRecords = table.data.length;
        }
        table.pagination.currentPage = 1;
        table.columns = Call.getColumns();
        this.store.callTable.next(table);
      }
    });
  }

  changePage(page: number) {
    let table = new Table().copy(this.store.callTable.value);
    table.pagination.currentPage = page;
    if (table.lazyLoad) {
      // service call
    } else {

    }
    this.store.callTable.next(table);
  }

  changeRecordsPerPage(recordsPerPage: number) {
    let table = new Table().copy(this.store.callTable.value);
    table.pagination.recordsPerPage = recordsPerPage;
    this.store.callTable.next(table);
  }

  changeSort(col: string) {
    let table = new Table().copy(this.store.callTable.value);
    let columns = table.columns;
    columns.filter(column => column.value !== col).forEach(column => column.sort = Column.SORT_NONE);
    columns.filter(column => column.value === col).forEach(column => {
      column.sort = ((column.sort + 2) % 3) - 1;
      table.sorting.order = column.sort;
      table.sorting.column = column.sort === Column.SORT_NONE ? null : column.value;
    });
    this.store.callTable.next(table);
  }

  filter(col: string, query: string) {
    let table = new Table().copy(this.store.callTable.value);
    table.columns.filter(column => column.value === col).forEach(column => column.filter = query);
    table.filtering.filters = table.columns.filter(column => column.filter && column.filter.trim().length >= 0).map(column => { return { column: column.value, value: column.filter } })
    this.store.callTable.next(table);
  }


  updateCustomer(customerDto: any) {

    let url = Uriconstant.END_POINT + Uriconstant.UPDATE_CALL //+ "_currentPage=" + 1 + "&&_recordsPerPage=" + 10 + "&&_totalPages=";//"http://localhost/shuddhi/GetAllCallController/list?_currentPage=1&&_recordsPerPage=2&&_totalPages=";
    this.httpClientWrapper.put(url, customerDto).subscribe(response => {
      if (response) {
        console.log("response========================>", response);
      }
    });

  }

  saveCustomer(customerDto: any) {

    let url = Uriconstant.END_POINT + Uriconstant.CREATE_CALL //+ "_currentPage=" + 1 + "&&_recordsPerPage=" + 10 + "&&_totalPages=";//"http://localhost/shuddhi/GetAllCallController/list?_currentPage=1&&_recordsPerPage=2&&_totalPages=";
    this.httpClientWrapper.post(url, customerDto).subscribe(response => {
      if (response) {
        console.log("response========================>", response);
      }
    });
  }
}

const fixture = {
  lazyLoad: false,
  totalRecords: 13,
  data: [
    new Call().copy(<Call>{ id: 1, address: "48, Janak Nagar, Nr Jay Jalaram Nagar, Bh Mother School,", area: "Gotri Road", city: "Vadodara", phoneNo: "7984850253", description: "", active: true }),
  ]
}

export const getList = { "pagination": { "from": null, "to": 2 }, "sort": { "coulmn": null, "order": null }, "filters": [{ "coulmn": null, "value": null }] }