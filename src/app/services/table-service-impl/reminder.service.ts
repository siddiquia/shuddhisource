import { Injectable } from '@angular/core';
import { TableService } from './table-service';
import { StoreService } from '../store.service';
import { Table } from 'src/app/components/table/table';
import { Reminder } from 'src/app/pages/reminder-page/reminder';
import { Column } from 'src/app/components/table/column';

@Injectable({
  providedIn: 'root'
})
export class ReminderService implements TableService {

  constructor(public store: StoreService) {
    this.getData();
  }

  getData() {
    let table = new Table().copy(this.store.reminderTable.value);

    table.lazyLoad = fixture.lazyLoad;
    table.data = fixture.data;

    if (table.lazyLoad) {
      table.pagination.totalRecords = fixture.totalRecords;
    } else {
      table.pagination.totalRecords = table.data.length;
    }
    table.pagination.currentPage = 1;
    table.columns = Reminder.getColumns();
    this.store.reminderTable.next(table);
  }

  changePage(page: number) {
    let table = new Table().copy(this.store.reminderTable.value);
    table.pagination.currentPage = page;
    if (table.lazyLoad) {
      // service call
    } else {

    }
    this.store.reminderTable.next(table);
  }

  changeRecordsPerPage(recordsPerPage: number) {
    let table = new Table().copy(this.store.reminderTable.value);
    table.pagination.recordsPerPage = recordsPerPage;
    this.store.reminderTable.next(table);
  }

  changeSort(col: string) {
    let table = new Table().copy(this.store.reminderTable.value);
    let columns = table.columns;
    columns.filter(column => column.value !== col).forEach(column => column.sort = Column.SORT_NONE);
    columns.filter(column => column.value === col).forEach(column => {
      column.sort = ((column.sort + 2) % 3) - 1;
      table.sorting.order = column.sort;
      table.sorting.column = column.sort === Column.SORT_NONE ? null : column.value;
    });
    this.store.reminderTable.next(table);
  }

  filter(col: string, query: string) {
    let table = new Table().copy(this.store.reminderTable.value);
    table.columns.filter(column => column.value === col).forEach(column => column.filter = query);
    table.filtering.filters = table.columns.filter(column => column.filter && column.filter.trim().length >= 0).map(column => { return { column: column.value, value: column.filter } })
    this.store.reminderTable.next(table);
  }

}

const fixture = {
  lazyLoad: false,
  totalRecords: 13,
  data: [
    new Reminder().copy(<Reminder>{ id: 1, address: "48, Janak Nagar, Nr Jay Jalaram Nagar, Bh Mother School,", area: "Gotri Road", city: "Vadodara", phoneNo: "7984850253", description: "", active: true }),
  ]
}