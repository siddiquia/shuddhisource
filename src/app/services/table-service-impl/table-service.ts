
export interface TableService {

    getData();

    changePage(page: number);

    changeRecordsPerPage(recordsPerPage: number);

    changeSort(col:string);

    filter(col: string, query: string);

}