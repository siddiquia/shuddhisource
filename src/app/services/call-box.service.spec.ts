import { TestBed } from '@angular/core/testing';

import { CallBoxService } from './call-box.service';

describe('CallBoxService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CallBoxService = TestBed.get(CallBoxService);
    expect(service).toBeTruthy();
  });
});
