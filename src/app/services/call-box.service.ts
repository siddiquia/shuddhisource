import { Injectable } from '@angular/core';
import { StoreService } from './store.service';
import { CallBox } from '../components/call-box/call-box';

@Injectable({
  providedIn: 'root'
})
export class CallBoxService {

  constructor(public store: StoreService) {
    this.getCallBoxesData();
  }

  getCallBoxesData() {
    let callBox = new CallBox().copy(this.store.callBox.value);
    callBox.total = 5476;
    callBox.unassigned = 44;
    callBox.pending = 24;
    callBox.todays = 10;
    callBox.todaysCompleted = 17;
    callBox.totalCompleted = 5478;
    this.store.callBox.next(callBox);
  }

}
