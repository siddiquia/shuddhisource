import { Injectable } from '@angular/core';
import { StoreService } from './store.service';
import { NavigationStart } from '@angular/router';
import { SideMenu } from '../components/side-menu/side-menu';
import { MenuItem } from '../components/side-menu/menu-item';
import { PageName } from '../components/page-name/page-name';
import { NavigationService } from './navigation.service';

@Injectable({
  providedIn: 'root'
})
export class SideMenuService {

  constructor(
    public store: StoreService,
    public navigation: NavigationService
  ) {

    this.navigation.router.events.forEach(event => {
      if (event instanceof NavigationStart) {
        let sideMenu = new SideMenu().copy(this.store.sideMenu.value);
        let pageName = new PageName().copy(this.store.pageName.value);
        pageName.submenu = null;
        sideMenu.menuItems.forEach(menuItem => {
          menuItem.active = false;
          if (event.url.startsWith(menuItem.link)) {
            pageName.main = menuItem.text;
            menuItem.active = true;
          } else {
            if (menuItem.subMenu.length > 0) {
              menuItem.subMenu.forEach(subMenuItem => {
                if (event.url.startsWith(subMenuItem.link)) {
                  pageName.main = menuItem.text;
                  pageName.submenu = subMenuItem.text;
                  menuItem.active = true;
                }
              })
            }
          }
        });
        this.store.pageName.next(pageName);
        this.store.sideMenu.next(sideMenu);
      }
    });

    let sideMenu = new SideMenu();
    sideMenu.menuItems.push(new MenuItem().copy(<MenuItem>{
      icon: "fas fa-toolbox", text: "Masters", subMenu: [
        new MenuItem().copy(<MenuItem>{ icon: "far fa-circle", text: "Firm", subMenu: [], dropdown: false, active: false, link: "/masters/firm" }),
        new MenuItem().copy(<MenuItem>{ icon: "far fa-circle", text: "Unit", subMenu: [], dropdown: false, active: false, link: "/masters/unit" }),
        new MenuItem().copy(<MenuItem>{ icon: "far fa-circle", text: "Job Type", subMenu: [], dropdown: false, active: false, link: "/masters/job-type" }),
        new MenuItem().copy(<MenuItem>{ icon: "far fa-circle", text: "Employee", subMenu: [], dropdown: false, active: false, link: "/masters/employee" }),
        new MenuItem().copy(<MenuItem>{ icon: "far fa-circle", text: "Customer", subMenu: [], dropdown: false, active: false, link: "/masters/customer" }),
        new MenuItem().copy(<MenuItem>{ icon: "far fa-circle", text: "Role", subMenu: [], dropdown: false, active: false, link: "/masters/role" })
        // new MenuItem().copy(<MenuItem>{ icon: "far fa-circle", text: "Bulk Upload", subMenu: [], dropdown: false, active: false, link: "/masters/bulk-upload" })
      ], dropdown: false, active: false, link: null
    }));
    sideMenu.menuItems.push(new MenuItem().copy(<MenuItem>{
      icon: "fas fa-phone", text: "Calls", subMenu: [
        new MenuItem().copy(<MenuItem>{ icon: "far fa-circle", text: "Call", subMenu: [], dropdown: false, active: false, link: "/calls/call" }),
        new MenuItem().copy(<MenuItem>{ icon: "far fa-circle", text: "Reminder", subMenu: [], dropdown: false, active: false, link: "/calls/reminder" }),
      ], dropdown: false, active: false, link: null
    }));
    sideMenu.menuItems.push(new MenuItem().copy(<MenuItem>{ icon: "fas fa-cogs", text: "Configuration", subMenu: [], dropdown: false, active: false, link: "/config/access-control" }));
    sideMenu.menuItems.push(new MenuItem().copy(<MenuItem>{ icon: "fas fa-tachometer-alt", text: "Dashboard", subMenu: [], dropdown: false, active: false, link: "/dashboard" }));

    this.store.sideMenu.next(sideMenu);
  }

  toggleSideMenu() {
    let sideMenu = new SideMenu().copy(this.store.sideMenu.value);
    sideMenu.menuOpen = !sideMenu.menuOpen;
    if (!sideMenu.menuOpen) {
      sideMenu.menuItems.forEach(item => item.dropdown = false);
    }
    this.store.sideMenu.next(sideMenu);
  }

  toggleSubMenu(index: number) {
    let sideMenu = new SideMenu().copy(this.store.sideMenu.value);
    sideMenu.menuItems[index].dropdown = !sideMenu.menuItems[index].dropdown;
    this.store.sideMenu.next(sideMenu);
  }

  menuItemClick(index: number) {
    let sideMenu = new SideMenu().copy(this.store.sideMenu.value);

    let selectedItem = sideMenu.menuItems[index];

    if (selectedItem.subMenu.length > 0) {
      sideMenu.menuOpen = true;
      selectedItem.dropdown = !selectedItem.dropdown;
      this.store.sideMenu.next(sideMenu);
    } else {
      sideMenu.menuOpen = false;
      sideMenu.menuItems.forEach(item => { item.dropdown = false; item.active = false });
      this.store.sideMenu.next(sideMenu);
      this.navigation.navigateTo([selectedItem.link]);
    }
  }

  subMenuClick(index: number, jndex: number) {
    let sideMenu = new SideMenu().copy(this.store.sideMenu.value);
    let selectedSubItem = sideMenu.menuItems[index].subMenu[jndex];

    sideMenu.menuOpen = false;
    sideMenu.menuItems.forEach(item => { item.dropdown = false; item.active = false });

    this.store.sideMenu.next(sideMenu);
    this.navigation.navigateTo([selectedSubItem.link]);
  }

}
