import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams, HttpErrorResponse } from '@angular/common/http';
//import { ErrorObservable } from 'rxjs/observable/ErrorObservable';
import { catchError, tap } from 'rxjs/operators';
//import { of } from 'rxjs/observable/of';
//import { ErrorMessageModalComponent } from './error-message/error-message-modal.component';
import { Observable, of } from 'rxjs';

@Injectable()
export class HttpClientWrapper {
    private httpOptions = {
        headers: new HttpHeaders({
            //'Content-Type': 'application/json',
            'Accept': 'application/json'  
        }) 
    }; 
    constructor(@Inject(HttpClient) private httpClient: HttpClient) { }

    public get(serviceUri: string, parameters?: {}, option?: {}) {
        const httpOptions = Object.assign({}, this.httpOptions);
        if (parameters) {
            let httpParams = new HttpParams();
            Object.keys(parameters).forEach(key => {
                httpParams = httpParams.append(key, parameters[key]);
            });
            httpOptions["params"] = httpParams;
        }
        return this.httpClient.get(serviceUri, httpOptions)
            .pipe(catchError(this.handleError));
    }

    public put(serviceUri: string, body?: any | null, parameters?: {}, option?: {}) {
        const httpOptions = Object.assign({}, this.httpOptions);
        if (parameters) {
            let httpParams = new HttpParams();
            Object.keys(parameters).forEach(key => {
                httpParams = httpParams.append(key, parameters[key]);
            });
            httpOptions["params"] = httpParams;
        }
        return this.httpClient.put(serviceUri, body, httpOptions)
            .pipe(catchError(this.handleError));
    }

    public post(serviceUri: string, body: any | null, parameters?: {}, option?: {}) {
        const httpOptions = Object.assign({}, this.httpOptions);
        if (parameters) {
            let httpParams = new HttpParams();
            Object.keys(parameters).forEach(key => {
                httpParams = httpParams.append(key, parameters[key]);
            });
            httpOptions["params"] = httpParams;
        }
        console.log("service posted body==>",body);
        return this.httpClient.post(serviceUri, body, httpOptions)
            .pipe(catchError(this.handleError));
    }

    public delete(serviceUri: string, parameters?: {}, option?: {}) {
        const httpOptions = Object.assign({}, this.httpOptions);
        if (parameters) {
            let httpParams = new HttpParams();
            Object.keys(parameters).forEach(key => {
                httpParams = httpParams.append(key, parameters[key]);
            });
            httpOptions["params"] = httpParams;
        }
        return this.httpClient.delete(serviceUri, httpOptions)
            .pipe(catchError(this.handleError));
    }
 
    public downloadFile(url: string, body?: {}, parameters?: {}): Observable<any> {
        const httpOptions = Object.assign({}, this.httpOptions);
        httpOptions["responseType"] = "blob";
        httpOptions["observe"] = "response";
        if (parameters) {
            let httpParams = new HttpParams();
            Object.keys(parameters).forEach(key => {
                httpParams = httpParams.append(key, parameters[key]);
            });
            httpOptions["params"] = httpParams;
        }

        //added below if condition for ASUP bundle download which requires a post type request with ASUP data
        if (body) {
            return this.httpClient.post(url, body, httpOptions)
                .pipe(catchError(this.handleError));
        } else {
            // added the observe param so that we can fetch the entire response object
            // including the Response headers to the calling service
            return this.httpClient.get(url, httpOptions)
                .pipe(catchError(this.handleError));
        }
    }

    private handleError(errorResponse: HttpErrorResponse) { 
        // so need to map it accordingly and present it on UI
        let errorMessage = null;
        // fix for burt:1179851, the error can be null from backend in particular scenarios
        // need to handle NPE here.
        if(errorResponse.error) {
           errorMessage =  errorResponse.error.message || errorResponse.error;
        }
        
        return of({
            serviceErrorMessage: `${errorMessage}`
        });//new ErrorObservable(`Service failed with error code: ${errorResponse.status} : ${errorResponse.error}` );
    };


}