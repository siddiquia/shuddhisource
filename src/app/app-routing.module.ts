import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { CallPageComponent } from './pages/call-page/call-page.component';
import { ReminderPageComponent } from './pages/reminder-page/reminder-page.component';
import { AccessControlPageComponent } from './pages/access-control-page/access-control-page.component';
import { FirmPageComponent } from './pages/firm-page/firm-page.component';
import { UnitPageComponent } from './pages/unit-page/unit-page.component';
import { JobTypePageComponent } from './pages/job-type-page/job-type-page.component';
import { EmployeePageComponent } from './pages/employee-page/employee-page.component';
import { CustomerPageComponent } from './pages/customer-page/customer-page.component';
import { RolePageComponent } from './pages/role-page/role-page.component';
import { BulkUploadPageComponent } from './pages/bulk-upload-page/bulk-upload-page.component';
import { FirmAddPageComponent } from './pages/firm-add-page/firm-add-page.component';
import { CallAddPageComponent } from './pages/call-add-page/call-add-page.component';
import { ReminderAddPageComponent } from './pages/reminder-add-page/reminder-add-page.component';
import { UnitAddPageComponent } from './pages/unit-add-page/unit-add-page.component';
import { JobTypeAddPageComponent } from './pages/job-type-add-page/job-type-add-page.component';
import { EmployeeAddPageComponent } from './pages/employee-add-page/employee-add-page.component';
import { CustomerAddPageComponent } from './pages/customer-add-page/customer-add-page.component';
import { RoleAddPageComponent } from './pages/role-add-page/role-add-page.component';

const routes: Routes = [
  { path: '', redirectTo: '/dashboard', pathMatch: 'full' },
  { path: 'dashboard', component: DashboardComponent },
  { path: 'config/access-control', component: AccessControlPageComponent },
  { path: 'calls/call', component: CallPageComponent },
  { path: 'calls/call/add', component: CallAddPageComponent },
  { path: 'calls/reminder', component: ReminderPageComponent },
  { path: 'calls/reminder/add', component: ReminderAddPageComponent },
  { path: 'masters/firm', component: FirmPageComponent },
  { path: 'masters/firm/add', component: FirmAddPageComponent },
  { path: 'masters/unit', component: UnitPageComponent },
  { path: 'masters/unit/add', component: UnitAddPageComponent },
  { path: 'masters/job-type', component: JobTypePageComponent },
  { path: 'masters/job-type/add', component: JobTypeAddPageComponent },
  { path: 'masters/employee', component: EmployeePageComponent },
  { path: 'masters/employee/add', component: EmployeeAddPageComponent },
  { path: 'masters/customer', component: CustomerPageComponent },
  { path: 'masters/customer/add', component: CustomerAddPageComponent },
  { path: 'masters/role', component: RolePageComponent },
  { path: 'masters/role/add', component: RoleAddPageComponent },
  { path: 'masters/bulk-upload', component: BulkUploadPageComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
