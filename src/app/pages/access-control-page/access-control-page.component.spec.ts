import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccessControlPageComponent } from './access-control-page.component';

describe('AccessControlPageComponent', () => {
  let component: AccessControlPageComponent;
  let fixture: ComponentFixture<AccessControlPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccessControlPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccessControlPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
