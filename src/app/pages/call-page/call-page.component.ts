import { Component, OnInit } from '@angular/core';
import { CallService } from 'src/app/services/table-service-impl/call.service';
import { StoreService } from 'src/app/services/store.service';
import { NavigationService } from 'src/app/services/navigation.service';

@Component({
  selector: 'app-call-page',
  templateUrl: './call-page.component.html',
  styleUrls: ['./call-page.component.scss']
})
export class CallPageComponent implements OnInit {
  pageName:string;
  constructor(
    public store: StoreService,
    public service: CallService,
    public navigation: NavigationService
  ) { }

  ngOnInit() {
    this.pageName='Call Master'
  }

}
