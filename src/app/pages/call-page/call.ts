import { TableData } from "src/app/components/table/table-data";
import { Column } from "src/app/components/table/column";

export class Call implements TableData {
    public id: number;
    public callId: number;
    public status: string;
    public customerName: string;
    public address: string;
    public area: string;
    public city: string;
    public phoneNo: string;
    public jobType: string;
    public employeeName: string;
    public preferredTime: string;
    public amount: number;
    public rate: number;
    public reminderDate: string;
    public description: string;
    public active: boolean;

    public static getColumns(): Column[] {
        return [
            new Column().copy(<Column>{ name: "Call Id", value: "callID" }),
            new Column().copy(<Column>{ name: "Status", value: "status" }),
            new Column().copy(<Column>{ name: "Customer Name", value: "customerName" }),
            new Column().copy(<Column>{ name: "Address", value: "address" }),
            new Column().copy(<Column>{ name: "Area", value: "area" }),
            new Column().copy(<Column>{ name: "City", value: "city" }),
            new Column().copy(<Column>{ name: "Phone No", value: "phoneNo" }),
            new Column().copy(<Column>{ name: "Job Type", value: "jobType" }),
            new Column().copy(<Column>{ name: "Employee Name", value: "employeeName" }),
            new Column().copy(<Column>{ name: "Preferred Time", value: "preferredTime" }),
            new Column().copy(<Column>{ name: "Amount", value: "amount" }),
            new Column().copy(<Column>{ name: "Rate", value: "rate" }),
            new Column().copy(<Column>{ name: "Reminder Date", value: "reminderDate" }),
            new Column().copy(<Column>{ name: "Description", value: "description" }),
            new Column().copy(<Column>{ name: "Active", value: "active" })
        ];
    }

    constructor() {
        this.id = 0;
        this.callId = 0;
        this.status = "";
        this.customerName = "";
        this.address = "";
        this.area = "";
        this.city = "";
        this.phoneNo = "";
        this.jobType = "";
        this.employeeName = "";
        this.preferredTime = "";
        this.amount = 0;
        this.rate = 0;
        this.reminderDate = "";
        this.description = "";
        this.active = false;
    }

    copy(call: Call) {
        return Object.assign(this, call);
    }
}