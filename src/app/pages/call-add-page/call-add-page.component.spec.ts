import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CallAddPageComponent } from './call-add-page.component';

describe('CallAddPageComponent', () => {
  let component: CallAddPageComponent;
  let fixture: ComponentFixture<CallAddPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CallAddPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CallAddPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
