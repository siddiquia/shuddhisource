
export class EmployeeDTO {
    /* public id: number;
    public userName: string;
    public name: string;
    public permanentAddress: string;
    public mobileNo: string;
    public phoneNo: string;
    public emailId: string;
    public active: boolean;  
    public joineDate:string;
    public roleId:number;
    public presentAddress:string;
    public age:number;
    public sex:string;
    public password:string;
    public fcmId:number;
    public phoneInfo:string;
    public isCreateRights:boolean;
    public isDeleteRights:boolean;
    public isEditRights:boolean;
    public isActive:boolean;
    public isDeleted:boolean;
    public createdby:string;
    public updatedby:string
    public birthdate:string
    public isExportRights:boolean;
    public createdOn:string;
    public isViewRights:boolean;
    public updatedOn:string
    public imei:string; */

    public id: number;
    public name: string
    public fatherName: string;
    public joineDate: string;
    public roleId: number;
    public presentAddress: string;
    public permanentAddress: string;
    public phoneNo: string;
    public mobileNo: string;
    public emailId: string;
    public age: number;
    public sex: string;
    public userName: string;
    public password: string;
    public fcmId: string;
    public phoneInfo: string;
    public isCreateRights: boolean;
    public isDeleteRights: boolean;
    public isEditRights: boolean;
    public isActive: boolean;
    public isDeleted: boolean;
    public createdby: string;
    public updatedby: string;
    public birthdate: string;
    public isExportRights: boolean;
    public isMACId: number;
    public updatedOn: string;
    public imei: string;
    public isViewRights: boolean
    public createdOn: string;
    constructor() { }

}