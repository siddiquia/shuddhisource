import { TableData } from "src/app/components/table/table-data";
import { Column } from "src/app/components/table/column";

export class Employee implements TableData {
    public id: number;
    public userName: string;
    public name: string;
    public permanentAddress: string;
    public mobileNo: string;
    public phoneNo: string;
    public emailId: string;
    public active: boolean;

    public static getColumns(): Column[] {
        return [
            new Column().copy(<Column>{ name: "UserName", value: "userName" }),
            new Column().copy(<Column>{ name: "Name", value: "name" }),
            new Column().copy(<Column>{ name: "Permanent Address", value: "permanentAddress" }),
            new Column().copy(<Column>{ name: "Mobile No", value: "mobileNo" }),
            new Column().copy(<Column>{ name: "Phone No", value: "phoneNo" }),
            new Column().copy(<Column>{ name: "Email Id", value: "emailId" }),
            new Column().copy(<Column>{ name: "Active", value: "active" })
        ];
    }

    constructor() {
        this.id = 0;
        this.userName = "";
        this.name = "";
        this.permanentAddress = "";
        this.mobileNo = "";
        this.phoneNo = "";
        this.emailId = "";
        this.active = false;
    }

    copy(employee: Employee) {
        return Object.assign(this, employee);
    }
}