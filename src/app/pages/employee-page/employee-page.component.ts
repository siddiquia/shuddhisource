import { Component, OnInit } from '@angular/core';
import { EmployeeService } from 'src/app/services/table-service-impl/employee.service';
import { StoreService } from 'src/app/services/store.service';
import { NavigationService } from 'src/app/services/navigation.service'; 
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Employee } from './employee';
import { EmployeeDTO } from './employee-dto';
@Component({
  selector: 'app-employee-page',
  templateUrl: './employee-page.component.html',
  styleUrls: ['./employee-page.component.scss']
})
export class EmployeePageComponent implements OnInit {
  closeResult: string;
  registerForm: FormGroup;
    submitted = false;
    employeeDTO:EmployeeDTO=new EmployeeDTO();
   constructor(
    private store: StoreService,
    private service: EmployeeService
  ) { }

  ngOnInit() {
    /* this.registerForm = this.formBuilder.group({
      fullName: ['', Validators.required],
      fatherName: ['', Validators.required],
      joiningDate: ['', Validators.required],
      address: ['', Validators.required],
      address2: ['', Validators.required],
      mobileNo: ['', Validators.required],
      phoneNo: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      birthDate:  ['', Validators.required],
      sex:  ['', Validators.required],
      userName:  ['', Validators.required],
      password: ['', [Validators.required, Validators.minLength(6)]],
      role: ['', Validators.required],
      isActive:[false],
      isCreateRights:[false],
      isEditRights:[false],
      isViewRights:[false],
      isDeleted:[false],
      isExportRights:[false],

  }); */
}

// convenience getter for easy access to form fields
get f() { return this.registerForm.controls; }
onSubmit() {
  this.submitted = true;
  console.log("------>",this.registerForm.controls);
  // stop here if form is invalid
  /* if (this.registerForm.invalid) {
      return;
  } */
  
  this.employeeDTO.id=0;
  this.employeeDTO.name=this.registerForm.controls.fullName.value
  this.employeeDTO.fatherName=this.registerForm.controls.fatherName.value
  this.employeeDTO.joineDate=this.registerForm.controls.joiningDate.value
  this.employeeDTO.permanentAddress=this.registerForm.controls.address2.value
  this.employeeDTO.presentAddress=this.registerForm.controls.address.value
  this.employeeDTO.mobileNo=this.registerForm.controls.mobileNo.value
  this.employeeDTO.phoneNo=this.registerForm.controls.phoneNo.value
  this.employeeDTO.emailId=this.registerForm.controls.email.value
  this.employeeDTO.birthdate=this.registerForm.controls.birthDate.value
  this.employeeDTO.sex=this.registerForm.controls.sex.value
  this.employeeDTO.userName=this.registerForm.controls.userName.value
  this.employeeDTO.password=this.registerForm.controls.password.value
  this.employeeDTO.roleId=this.registerForm.controls.role.value
  this.employeeDTO.isActive=this.registerForm.controls.isActive.value 
  this.employeeDTO.isCreateRights=this.registerForm.controls.isCreateRights.value
  this.employeeDTO.isEditRights=this.registerForm.controls.isEditRights.value
  this.employeeDTO.isViewRights=this.registerForm.controls.isViewRights.value
  this.employeeDTO.isDeleted=this.registerForm.controls.isDeleted.value
  this.employeeDTO.isExportRights=this.registerForm.controls.isExportRights.value
  this.employeeDTO.isMACId=0;
  this.employeeDTO.imei="";
  this.employeeDTO.age=0;
  console.log("===================>",empJson);
  this.service.saveEmployee(empJson);
  alert('SUCCESS!! :-)');
}
 /*  open(content) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title', size: 'lg'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }
 */
  /* private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }
 */
}
export const empJson=
  {   "id":0,
   "name":"aaaa",
   "fatherName":"dilipkumar",
   "joineDate":null,
   "roleId":"1",
   "presentAddress":"a",
   "permanentAddress":"a",
   "phoneNo":null,
   "mobileNo":"9428120749",
   "emailId":"nikunj.ce31@gmail.com",
   "age":0,
   "sex":"1",
   "userName":"nikunj",
   "password":"rathod",
   "fcmId":"sample string 4",
   "phoneInfo":"sample string 6",
   "isCreateRights":1,
   "isDeleteRights":1,
   "isEditRights":1,
   "isActive":1,
   "isDeleted":0,
   "createdby":"0",
   "updatedby":null,
   "birthdate":null,
   "isExportRights":1,
   "isMACId":0,
   "createdOn":1489257000000,
   "updatedOn":null,
   "imei":"sample string 5",
   "isViewRights":1
}
