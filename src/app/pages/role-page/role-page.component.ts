import { Component, OnInit } from '@angular/core';
import { RoleService } from 'src/app/services/table-service-impl/role.service';
import { StoreService } from 'src/app/services/store.service';
import { NavigationService } from 'src/app/services/navigation.service';

@Component({
  selector: 'app-role-page',
  templateUrl: './role-page.component.html',
  styleUrls: ['./role-page.component.scss']
})
export class RolePageComponent implements OnInit {

  constructor(
    public store: StoreService,
    public service: RoleService,
    public navigation: NavigationService
  ) { }

  ngOnInit() {
  }

}
