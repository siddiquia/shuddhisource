import { TableData } from "src/app/components/table/table-data";
import { Column } from "src/app/components/table/column";

export class Role implements TableData {
    public id: number;
    public name: string;
    public description: string;
    public active: boolean;

    public static getColumns(): Column[] {
        return [
            new Column().copy(<Column>{ name: "Name", value: "name" }),
            new Column().copy(<Column>{ name: "Description", value: "description" }),
            new Column().copy(<Column>{ name: "Active", value: "active" })
        ];
    }

    constructor() {
        this.id = 0;
        this.name = "";
        this.description = "";
        this.active = false;
    }

    copy(role: Role) {
        return Object.assign(this, role);
    }
}