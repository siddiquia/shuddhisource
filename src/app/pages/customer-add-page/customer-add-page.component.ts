import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { Form, FormControlInput, FormControlTextarea, FormControlCheckbox } from 'src/app/components/form/form';
import { NavigationService } from 'src/app/services/navigation.service';
import { HttpClientWrapper } from '../../HttpClientWrapper';
import { Uriconstant } from '../../uri-constant';
import { CustomerService } from '../../services/table-service-impl/customer.service';

@Component({
  selector: 'app-customer-add-page',
  templateUrl: './customer-add-page.component.html',
  styleUrls: ['./customer-add-page.component.scss']
})
export class CustomerAddPageComponent implements OnInit {

  public form: Form;
  @Input() customerSelectedId: string
  @Input() selectedIDForEdit;
  name = new FormControlInput();
  constructor(public navigation: NavigationService, public httpClientWrapper: HttpClientWrapper,private customerService:CustomerService) {

    let name = new FormControlInput();
    name.placeholder = "Name";
    name.label = "Name";
    name.name = "name";
    name.type = "text";
    name.required = true;
    name.value = ""

    let address1 = new FormControlTextarea();
    address1.name = "address1";
    address1.placeholder = "Address1";
    address1.label = "Address1";
    address1.rows = 4;
    address1.value = ""
    let address2 = new FormControlTextarea();
    address2.name = "address2";
    address2.placeholder = "Address2";
    address2.label = "Address2";
    address2.rows = 4;
    address1.value = ""
    let address3 = new FormControlTextarea();
    address3.name = "address3";
    address3.placeholder = "Address3";
    address3.label = "Address3";
    address3.rows = 4;
    address3.value = "";

    let area = new FormControlTextarea();
    area.name = "area";
    area.placeholder = "Area";
    area.label = "Area";
    area.rows = 4;
    area.value = "";
    let city = new FormControlInput();
    city.name = "city";
    city.placeholder = "City";
    city.label = "City";
    city.type = "text";
    city.value = ""
    let pincode = new FormControlInput();
    pincode.name = "pincode";
    pincode.placeholder = "Pin Code";
    pincode.label = "Pin Code";
    pincode.type = "text";
    pincode.value = ""

    /*    let contactPersonName = new FormControlInput();
       contactPersonName.placeholder = "Contact Person Name";
       contactPersonName.label = "Contact Person Name";
       contactPersonName.name = "contactPersonName";
       contactPersonName.type = "text"; */

    let phoneNo1 = new FormControlInput();
    phoneNo1.placeholder = "Phone1";
    phoneNo1.label = "Phone1";
    phoneNo1.name = "phone1";
    phoneNo1.type = "text";
    phoneNo1.value = ""
    let phoneNo2 = new FormControlInput();
    phoneNo2.placeholder = "Phone2";
    phoneNo2.label = "Phone2";
    phoneNo2.name = "phoneNo2";
    phoneNo2.type = "text";
    phoneNo2.value = ""

    let phoneNo3 = new FormControlInput();
    phoneNo3.placeholder = "Phone3";
    phoneNo3.label = "Phone3";
    phoneNo3.name = "phoneNo3";
    phoneNo3.type = "text";
    phoneNo3.value = ""

    let emailId = new FormControlInput();
    emailId.placeholder = "Email Id";
    emailId.label = "Email Id";
    emailId.name = "emailId";
    emailId.type = "text";
    emailId.value = ""

    let description = new FormControlTextarea();
    description.name = "description";
    description.placeholder = "Description";
    description.label = "Description";
    description.rows = 4;
    description.value = ""
    let active = new FormControlCheckbox();
    active.label = "Active";
    active.name = "active";


    this.form = new Form();
    this.form.name = "firm";
    this.form.submit = "Save";
    this.form.cancel = null;
    this.form.inputs = [
      name, address1, address2, address3, area, city, pincode, phoneNo1, phoneNo2, phoneNo3, emailId, description, active
    ]

  }

  ngOnInit() {
    let url = Uriconstant.END_POINT + Uriconstant.GET_CUSTOMER_LIST_BY_ID //+ "_currentPage=" + 1 + "&&_recordsPerPage=" + 10 + "&&_totalPages=";//"http://localhost/shuddhi/GetAllCallController/list?_currentPage=1&&_recordsPerPage=2&&_totalPages=";

   /*  this.form.inputs.forEach(obj => {
      console.log("obj---->",obj);
    }) */
    //this.form.inputs["FormControlInput"].name="akram"
    this.httpClientWrapper.get(url, { id: 48 }).subscribe(response => {
      if (response) {
        console.log(this.form, "response s", response);
        this.form.inputs.forEach(obj => { 
          if(obj.name=='name'){
            console.log("obj---123->",response["payLoad"].data[0])
            obj.value=response["payLoad"].data[0].name
          }else if(obj.name=='address1'){
            obj.value=response["payLoad"].data[0].address1
          }else if(obj.name=='address2'){
            obj.value=response["payLoad"].data[0].address2
          }else if(obj.name=='address3'){
            obj.value=response["payLoad"].data[0].address3
          }else if(obj.name=='area'){
            obj.value=response["payLoad"].data[0].area
          }else if(obj.name=='city'){
            obj.value=response["payLoad"].data[0].city
          }else if(obj.name=='phincode'){
            obj.value=response["payLoad"].data[0].phincode
          }else if(obj.name=='phoneNo1'){
            obj.value=response["payLoad"].data[0].phone1
          }else if(obj.name=='phoneNo2'){
            obj.value=response["payLoad"].data[0].phone2
          }else if(obj.name=='phoneNo3'){
            obj.value=response["payLoad"].data[0].phone3
          }else if(obj.name=='emailId'){
            obj.value=response["payLoad"].data[0].emailid
          }else if(obj.name=='description'){
            obj.value=response["payLoad"].data[0].description
          }
        }) 
      }
    });
  }
  getselectedIDForEdit(event) {
    console.log("in Add page --", event);
  }
  onSubmit(formData) {
    
    if (formData) {
      console.log("formData response s", formData);
      formData.forEach(obj => {
        console.log("obj==========================================>",obj);
        if(obj.name=='name'){
          console.log("obj---123->",obj.value)
          cutomerDTO.name=obj.value;
        }else if(obj.name=='address1'){
          cutomerDTO.address1=obj.value
        }else if(obj.name=='address2'){
          cutomerDTO.address2=obj.value
        }else if(obj.name=='address3'){
          cutomerDTO.address3=obj.value
        }else if(obj.name=='area'){
          cutomerDTO.area=obj.value
        }else if(obj.name=='city'){
          cutomerDTO.city=obj.value
        }else if(obj.name=='phincode'){
          cutomerDTO.phincode=obj.value
        }else if(obj.name=='phone1'){
          cutomerDTO.phone1=obj.value
        }else if(obj.name=='phoneNo2'){
          cutomerDTO.phone2=obj.value
        }else if(obj.name=='phoneNo3'){
          cutomerDTO.phone3=obj.value
        }else if(obj.name=='emailId'){
          cutomerDTO.emailid=obj.value
        }else if(obj.name=='description'){
          cutomerDTO.description=obj.value
        }
      }) 
    } 
   
    if(cutomerDTO.id==49){
      cutomerDTO.id=48 
      this.customerService.updateCustomer(cutomerDTO);
    }else{
      cutomerDTO.id=0;
      this.customerService.saveCustomer(cutomerDTO);
    }
    
  }

}
export const cutomerDTO={  
  "id":null,
  "name":"Test123",
  "address1":null,
  "address2":null,
  "address3":null,
  "phone1":null,
  "phone2":null,
  "phone3":null,
  "emailid":null,
  "area":null,
  "description":null,
  "city":null,
  "phincode":null,
  
}
/* "latitude":null,
  "longitude":null,
  "isactive":0,
  "isdeleted":0,
  "createdby":null,
  "updatedby":null,
  "createdOn":null,
  "updatedOn":null */