import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RoleAddPageComponent } from './role-add-page.component';

describe('RoleAddPageComponent', () => {
  let component: RoleAddPageComponent;
  let fixture: ComponentFixture<RoleAddPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RoleAddPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RoleAddPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
