import { Component, OnInit } from '@angular/core';
import { Form, FormControlInput, FormControlTextarea, FormControlCheckbox } from 'src/app/components/form/form';
import { NavigationService } from 'src/app/services/navigation.service';

@Component({
  selector: 'app-firm-add-page',
  templateUrl: './firm-add-page.component.html',
  styleUrls: ['./firm-add-page.component.scss']
})
export class FirmAddPageComponent implements OnInit {

  public form: Form;

  constructor(public navigation: NavigationService) {

    let name = new FormControlInput();
    name.placeholder = "Name";
    name.label = "Name";
    name.name = "name";
    name.type = "text";
    name.required = true;

    let address = new FormControlTextarea();
    address.name = "address";
    address.placeholder = "Address";
    address.label = "Address";
    address.rows = 4;

    let contactPersonName = new FormControlInput();
    contactPersonName.placeholder = "Contact Person Name";
    contactPersonName.label = "Contact Person Name";
    contactPersonName.name = "contactPersonName";
    contactPersonName.type = "text";

    let mobileNo = new FormControlInput();
    mobileNo.placeholder = "Mobile No";
    mobileNo.label = "Mobile No";
    mobileNo.name = "mobileNo";
    mobileNo.type = "text";

    let phoneNo = new FormControlInput();
    phoneNo.placeholder = "Phone No";
    phoneNo.label = "Phone No";
    phoneNo.name = "phoneNo";
    phoneNo.type = "text";

    let emailId = new FormControlInput();
    emailId.placeholder = "Email Id";
    emailId.label = "Email Id";
    emailId.name = "emailId";
    emailId.type = "text";

    let description = new FormControlTextarea();
    description.name = "description";
    description.placeholder = "Description";
    description.label = "Description";
    description.rows = 4;

    let active = new FormControlCheckbox();
    active.label = "Active";
    active.name = "active";


    this.form = new Form();
    this.form.name = "firm";
    this.form.submit = "Save";
    this.form.cancel = null;
    this.form.inputs = [
      name, address, contactPersonName, mobileNo, phoneNo, emailId, description, active
    ]

  }

  ngOnInit() {
  }

  onSubmit(formData) {
    console.log(formData);
  }

}
