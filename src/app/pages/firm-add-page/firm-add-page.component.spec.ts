import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FirmAddPageComponent } from './firm-add-page.component';

describe('FirmAddPageComponent', () => {
  let component: FirmAddPageComponent;
  let fixture: ComponentFixture<FirmAddPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FirmAddPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FirmAddPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
