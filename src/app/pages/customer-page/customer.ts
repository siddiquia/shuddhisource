import { TableData } from "src/app/components/table/table-data";
import { Column } from "src/app/components/table/column";

export class Customer implements TableData {
    public id: number;
    public name: string;
    public address: string;
    public area: string;
    public city: string;
    public pincode: string;
    public phoneNo: string;
    public emailId: string;
    public description: string;
    public active: boolean;

    public static getColumns(): Column[] {
        return [
            new Column().copy(<Column>{ name: "Name", value: "name" }),
            new Column().copy(<Column>{ name: "Address", value: "address" }),
            new Column().copy(<Column>{ name: "Area", value: "area" }),
            new Column().copy(<Column>{ name: "City", value: "city" }),
            new Column().copy(<Column>{ name: "Pincode", value: "pincode" }),
            new Column().copy(<Column>{ name: "Phone No", value: "phoneNo" }),
            new Column().copy(<Column>{ name: "Email Id", value: "emailId" }),
            new Column().copy(<Column>{ name: "Description", value: "description" }),
            new Column().copy(<Column>{ name: "Active", value: "active" })
        ];
    }

    constructor() {
        this.id = 0;
        this.name = "";
        this.address = "";
        this.area = "";
        this.city = "";
        this.pincode = "";
        this.phoneNo = "";
        this.emailId = "";
        this.description = "";
        this.active = false;
    }

    copy(customer: Customer) {
        return Object.assign(this, customer);
    }
}