import { Component, OnInit } from '@angular/core';
import { Form, FormControlInput, FormControlTextarea, FormControlCheckbox, FormControlDropdown, FormControlCheckboxGroup } from 'src/app/components/form/form';
import { NavigationService } from 'src/app/services/navigation.service';

@Component({
  selector: 'app-employee-add-page',
  templateUrl: './employee-add-page.component.html',
  styleUrls: ['./employee-add-page.component.scss']
})
export class EmployeeAddPageComponent implements OnInit {

  public form: Form;

  constructor(public navigation: NavigationService) {

    let name = new FormControlInput();
    name.placeholder = "Name";
    name.label = "Full Name";
    name.name = "name";
    name.type = "text";
    name.required = true;

    let fatherName = new FormControlInput();
    fatherName.placeholder = "Father Name";
    fatherName.label = "Father Name";
    fatherName.name = "fatherName";
    fatherName.type = "text";

    let joiningDate = new FormControlInput();
    joiningDate.placeholder = "Joining Date";
    joiningDate.label = "Joining Date";
    joiningDate.name = "joiningDate";
    joiningDate.type = "date";

    let presentAddress = new FormControlTextarea();
    presentAddress.name = "presentAddress";
    presentAddress.placeholder = "Present Address";
    presentAddress.label = "Present Address";
    presentAddress.rows = 4;

    let permanentAddress = new FormControlTextarea();
    permanentAddress.name = "permanentAddress";
    permanentAddress.placeholder = "Permanent Address";
    permanentAddress.label = "Permanent Address";
    permanentAddress.rows = 4;

    let mobileNo = new FormControlInput();
    mobileNo.placeholder = "Mobile No";
    mobileNo.label = "Mobile No";
    mobileNo.name = "mobileNo";
    mobileNo.type = "text";

    let phoneNo = new FormControlInput();
    phoneNo.placeholder = "Phone No";
    phoneNo.label = "Phone No";
    phoneNo.name = "phoneNo";
    phoneNo.type = "text";

    let emailId = new FormControlInput();
    emailId.placeholder = "Email Id";
    emailId.label = "Email Id";
    emailId.name = "emailId";
    emailId.type = "text";

    let birthDate = new FormControlInput();
    birthDate.placeholder = "Birth Date";
    birthDate.label = "Birth Date";
    birthDate.name = "birthDate";
    birthDate.type = "date";

    let sex = new FormControlDropdown();
    sex.name = "sex";
    sex.label = "Sex";
    sex.placeholder = "Sex";
    sex.options = [{ value: "1", displayValue: "Male" }, { value: "0", displayValue: "Female" }];


    let userName = new FormControlInput();
    userName.placeholder = "User Name";
    userName.label = "User Name";
    userName.name = "userName";
    userName.type = "text";

    let password = new FormControlInput();
    password.placeholder = "Password";
    password.label = "Password";
    password.name = "password";
    password.type = "password";

    let roleName = new FormControlDropdown();
    roleName.name = "roleName";
    roleName.label = "Role Name";
    roleName.placeholder = "Role Name";
    roleName.options = [
      { value: "1", displayValue: "Super Admin" },
      { value: "2", displayValue: "Administrator" },
      { value: "3", displayValue: "Sales" },
      { value: "4", displayValue: "Support" },
      { value: "5", displayValue: "Back Office" }
    ];

    let active = new FormControlCheckbox();
    active.label = "Active";
    active.name = "active";

    let createRights = new FormControlCheckbox();
    createRights.label = "Create Rights";
    createRights.name = "createRights";

    let editRights = new FormControlCheckbox();
    editRights.label = "Edit Rights";
    editRights.name = "editRights";

    let viewRights = new FormControlCheckbox();
    viewRights.label = "View Rights";
    viewRights.name = "viewRights";

    let deleteRights = new FormControlCheckbox();
    deleteRights.label = "Delete Rights";
    deleteRights.name = "deleteRights";

    let exportRights = new FormControlCheckbox();
    exportRights.label = "Export Rights";
    exportRights.name = "deleteRights";

    let rights = new FormControlCheckboxGroup();
    rights.label = "Access Rights";
    rights.name = "rights";
    rights.checkboxes = [
      createRights, editRights, viewRights, deleteRights, exportRights
    ]

    this.form = new Form();
    this.form.name = "firm";
    this.form.submit = "Save";
    this.form.cancel = null;
    this.form.inputs = [
      name, fatherName, joiningDate, presentAddress, permanentAddress, mobileNo, phoneNo, emailId, birthDate, sex, userName, password, roleName, active, rights
    ]

  }

  ngOnInit() {
  }

  onSubmit(formData) {
    console.log(formData);
  }

}
