import { TableData } from "src/app/components/table/table-data";
import { Column } from "src/app/components/table/column";

export class Reminder implements TableData {
    public id: number;
    public customerName: string;
    public employeeName: string;
    public address: string;
    public area: string;
    public city: string;
    public phoneNo: string;
    public jobType: string;
    public firmName: string;
    public amount: number;
    public reminder: number;
    public reminderDate: string;
    public description: string;
    public lastCallTime: string;
    public active: boolean;

    public static getColumns(): Column[] {
        return [
            new Column().copy(<Column>{ name: "Customer Name", value: "customerName" }),
            new Column().copy(<Column>{ name: "Employee Name", value: "employeeName" }),
            new Column().copy(<Column>{ name: "Address", value: "address" }),
            new Column().copy(<Column>{ name: "Area", value: "area" }),
            new Column().copy(<Column>{ name: "City", value: "city" }),
            new Column().copy(<Column>{ name: "Phone No", value: "phoneNo" }),
            new Column().copy(<Column>{ name: "Job Type", value: "jobType" }),
            new Column().copy(<Column>{ name: "Firm", value: "firmName" }),
            new Column().copy(<Column>{ name: "Amount", value: "amount" }),
            new Column().copy(<Column>{ name: "Reminder", value: "reminder" }),
            new Column().copy(<Column>{ name: "Reminder Date", value: "reminderDate" }),
            new Column().copy(<Column>{ name: "Description", value: "description" }),
            new Column().copy(<Column>{ name: "Last Call Time", value: "lastCallTime" }),
            new Column().copy(<Column>{ name: "Active", value: "active" })
        ];
    }

    constructor() {
        this.id = 0;
        this.customerName = "";
        this.employeeName = "";
        this.address = "";
        this.area = "";
        this.city = "";
        this.phoneNo = "";
        this.jobType = "";
        this.firmName = "";
        this.amount = 0;
        this.reminder = 0;
        this.reminderDate = "";
        this.description = "";
        this.lastCallTime = "";
        this.active = false;
    }

    copy(reminder: Reminder) {
        return Object.assign(this, reminder);
    }
}