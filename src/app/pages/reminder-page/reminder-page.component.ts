import { Component, OnInit } from '@angular/core';
import { ReminderService } from 'src/app/services/table-service-impl/reminder.service';
import { StoreService } from 'src/app/services/store.service';
import { NavigationService } from 'src/app/services/navigation.service';

@Component({
  selector: 'app-reminder-page',
  templateUrl: './reminder-page.component.html',
  styleUrls: ['./reminder-page.component.scss']
})
export class ReminderPageComponent implements OnInit {

  constructor(
    public store: StoreService,
    public service: ReminderService,
    public navigation: NavigationService
  ) { }

  ngOnInit() {
  }

}
