import { TableData } from "src/app/components/table/table-data";
import { Column } from "src/app/components/table/column";

export class JobType implements TableData {
    public id: number;
    public name: string;
    public unitName: string;
    public description: string;
    public active: boolean;

    public static getColumns(): Column[] {
        return [
            new Column().copy(<Column>{ name: "Name", value: "name" }),
            new Column().copy(<Column>{ name: "Unit Name", value: "unitName" }),
            new Column().copy(<Column>{ name: "Description", value: "description" }),
            new Column().copy(<Column>{ name: "Active", value: "active" })
        ];
    }

    constructor() {
        this.id = 0;
        this.name = "";
        this.unitName = "";
        this.description = "";
        this.active = false;
    }

    copy(jobType: JobType) {
        return Object.assign(this, jobType);
    }
}