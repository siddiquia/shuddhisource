import { Component, OnInit } from '@angular/core';
import { JobTypeService } from 'src/app/services/table-service-impl/job-type.service';
import { StoreService } from 'src/app/services/store.service';
import { NavigationService } from 'src/app/services/navigation.service';

@Component({
  selector: 'app-job-type-page',
  templateUrl: './job-type-page.component.html',
  styleUrls: ['./job-type-page.component.scss']
})
export class JobTypePageComponent implements OnInit {

  constructor(
    public store: StoreService,
    public service: JobTypeService,
    public navigation: NavigationService
  ) { }

  ngOnInit() {
  }

}
