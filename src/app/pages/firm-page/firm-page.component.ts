import { Component, OnInit } from '@angular/core';
import { StoreService } from 'src/app/services/store.service';
import { FirmService } from 'src/app/services/table-service-impl/firm.service';
import { NavigationService } from 'src/app/services/navigation.service';

@Component({
  selector: 'app-firm-page',
  templateUrl: './firm-page.component.html',
  styleUrls: ['./firm-page.component.scss']
})
export class FirmPageComponent implements OnInit {

  constructor(
    public store: StoreService,
    public service: FirmService,
    public navigation: NavigationService
  ) {

  }

  ngOnInit() {
  }

}
