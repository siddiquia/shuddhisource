import { TableData } from "src/app/components/table/table-data";
import { Column } from "src/app/components/table/column";

export class Firm implements TableData {
    public id: number;
    public name: string;
    public address: string;
    public contactPerson: string;
    public mobileNo: string;
    public phoneNo: string;
    public emailId: string;
    public active: boolean;

    public static getColumns(): Column[] {
        return [
            new Column().copy(<Column>{ name: "Name", value: "name" }),
            new Column().copy(<Column>{ name: "Address", value: "address" }),
            new Column().copy(<Column>{ name: "Contact Person", value: "contactPerson" }),
            new Column().copy(<Column>{ name: "Mobile No", value: "mobileNo" }),
            new Column().copy(<Column>{ name: "Phone No", value: "phoneNo" }),
            new Column().copy(<Column>{ name: "Email Id", value: "emailId" }),
            new Column().copy(<Column>{ name: "Active", value: "active" })
        ];
    }

    constructor() {
        this.id = 0;
        this.name = "";
        this.address = "";
        this.contactPerson = "";
        this.mobileNo = "";
        this.phoneNo = "";
        this.emailId = "";
        this.active = false;
    }

    copy(firm: Firm) {
        return Object.assign(this, firm);
    }
}