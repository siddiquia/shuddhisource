import { Column } from "./column";


export class Sorting {
    public column: string;
    public order: number;

    constructor() {
        this.column = "";
        this.order = Column.SORT_NONE;
    }

    public copy(sorting: Sorting) {
        return Object.assign(this, sorting);
    }
}