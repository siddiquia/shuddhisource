export class Column {
    public static SORT_NONE = 0;
    public static SORT_ASC = 1;
    public static SORT_DESC = -1;

    public name: string;
    public value: string;
    public show: boolean;
    public sort: number;
    public sortable: boolean;
    public filter: string;
    public filterable: boolean;

    constructor() {
        this.name = "";
        this.value = "";
        this.show = true;
        this.sort = Column.SORT_NONE;
        this.sortable = true;
        this.filter = "";
        this.filterable = true;
    }

    public copy(col: Column) {
        return Object.assign(this, col);
    }

}
