import { TableData } from "./table-data";
import { Pagination } from "../pagination/pagination";
import { Sorting } from "./sorting";
import { Filtering } from "./filtering";
import { Column } from "./column";

export class Table {
    public lazyLoad: boolean;
    public data: TableData[];
    public pagination: Pagination;
    public sorting: Sorting;
    public filtering: Filtering;
    public columns: Column[];

    constructor() {
        this.lazyLoad = false;
        this.data = [];
        this.pagination = new Pagination();
        this.sorting = new Sorting();
        this.filtering = new Filtering();
        this.columns = [];
    }

    get displayData(): TableData[] {
        if (this.lazyLoad) {
            // everything already applied at backend
            // display as it is
            return this.data;
        }
        let displayData = this.data.filter(d => d);
        // apply at frontend filtering > sorting > pagination
        this.filtering.filters.forEach(filter => {
            displayData = displayData.filter(row => ("" + row[filter.column]).toLowerCase().match(filter.value));
        })
        displayData = this.sorting.column && this.sorting.order !== Column.SORT_NONE ? displayData.sort((a, b) => {
            var x = ("" + a[this.sorting.column]).toLowerCase();
            var y = ("" + b[this.sorting.column]).toLowerCase();
            if (x < y) { return this.sorting.order }
            if (x > y) { return this.sorting.order * -1 }
            return 0;
        }) : displayData;
        return displayData.slice(this.pagination.from - 1, this.pagination.to);
    }

    copy(table: Table) {
        return Object.assign(this, table);
    }
}