export class Form {

    public static LABEL_LEFT = 'left';
    public static LABEL_TOP = 'top';

    public name: string;
    public label: string;
    public cancel: string;
    public submit: string;
    public inputs: FormControl[];

    constructor() {
        this.label = Form.LABEL_TOP;
    }

}

export class FormControl {

    public static CONTROL_INPUT = 'input';
    public static CONTROL_TEXTAREA = 'textarea';
    public static CONTROL_CHECKBOX = 'checkbox';
    public static CONTROL_CHECKBOX_GROUP = 'checkbox-group';
    public static CONTROL_DROPDOWN = 'dropdown';
    public static CONTROL_RADIO = 'radio';

    public control: string;
    public name: string;
    public required: boolean;
    public label: string;
    public value: string;

    constructor() {
        this.required = false;
    }

}

export class FormControlInput extends FormControl {
    public type: string;
    public placeholder: string;
    constructor() {
        super();
        this.control = FormControl.CONTROL_INPUT;
        this.type = "text";
    }
}

export class FormControlTextarea extends FormControl {
    public rows: number;
    public placeholder: string;

    constructor() {
        super();
        this.control = FormControl.CONTROL_TEXTAREA;
        this.rows = 3;
    }
}

export class FormControlCheckboxGroup extends FormControl {

    public checkboxes: FormControlCheckbox[];

    constructor() {
        super();
        this.control = FormControl.CONTROL_CHECKBOX_GROUP;
        this.checkboxes = [];
    }

}

export class FormControlCheckbox extends FormControl {

    constructor() {
        super();
        this.control = FormControl.CONTROL_CHECKBOX;
    }
}

export class FormControlDropdown extends FormControl {
    public options: { value: string, displayValue: string }[];
    public placeholder: string;
    constructor() {
        super();
        this.control = FormControl.CONTROL_DROPDOWN;
        this.options = [];
    }
}