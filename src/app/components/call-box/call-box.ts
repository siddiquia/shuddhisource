export class CallBox {
    public total: number;
    public unassigned: number;
    public pending: number;
    public todays: number;
    public todaysCompleted: number;
    public totalCompleted: number;

    constructor() {
        this.total = 0;
        this.unassigned = 0;
        this.pending = 0;
        this.todays = 0;
        this.todaysCompleted = 0;
        this.totalCompleted = 0;
    }

    public copy(callBox: CallBox) {
        return Object.assign(this, callBox);
    }
}