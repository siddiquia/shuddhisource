import { Component, OnInit, Input } from '@angular/core';
import { Pagination } from './pagination';
import { TableService } from 'src/app/services/table-service-impl/table-service';

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.scss']
})
export class PaginationComponent implements OnInit {

  @Input('pagination') pagination: Pagination;
  @Input('service') service: TableService;

  constructor() {

  }

  ngOnInit() {
  }

  navigateTo(page: number) {
    this.service.changePage(page);
  }

  changeRecordsPerPage(event: Event) {
    this.service.changeRecordsPerPage(parseInt((<HTMLSelectElement>event.target).value));
  }

}
