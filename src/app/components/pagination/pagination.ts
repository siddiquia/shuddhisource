

export class Pagination {
    private _totalRecords: number;
    private _recordsPerPage: number;
    private _totalPages: number;
    private _currentPage: number;
    private _from: number;
    private _to: number;
    private _pages: number[];
    private _visiblePages: number[];
    public recordsPerPagesList: number[];


    constructor() {
        this._totalRecords = 0;
        this._recordsPerPage = 10;
        this._currentPage = 1;
        this.recordsPerPagesList = [10, 50, 100, 500];
        this.recalculate();
    }

    get totalRecords() {
        return this._totalRecords;
    }

    set totalRecords(totalRecords: number) {
        this._totalRecords = totalRecords;
        this.recalculate();
    }

    get recordsPerPage() {
        return this._recordsPerPage;
    }

    set recordsPerPage(recordsPerPage: number) {
        this._recordsPerPage = recordsPerPage;
        this.recalculate();
    }

    get totalPages() {
        return this._totalPages;
    }

    get currentPage() {
        return this._currentPage;
    }

    set currentPage(currentPage: number) {
        this._currentPage = currentPage < 1 ? 1 : currentPage > this._totalPages ? this._totalPages : currentPage;
        this.recalculate();
    }

    get from() {
        return this._from;
    }

    get to() {
        return this._to;
    }

    get pages() {
        return this._pages;
    }

    get visiblePages() {
        return this._visiblePages;
    }

    recalculate() {
        this._totalPages = Math.ceil(this._totalRecords / this._recordsPerPage);
        this._currentPage = this._currentPage > this._totalPages ? this._totalPages : this._currentPage;
        this._pages = [];
        for (let i = 1; i <= this._totalPages; i++) {
            this._pages.push(i);
        }
        this._visiblePages = this._pages.filter(page => ((page > this._currentPage - 3 && page < this._currentPage + 3) || (this._currentPage <= 4 && page <= 6) || (this._currentPage >= this._totalPages - 3 && page >= this._totalPages - 5)) && page !== 1 && page !== this._totalPages);
        this._from = (this._currentPage - 1) * this._recordsPerPage + 1;
        this._to = this._currentPage < this._totalPages ? this._currentPage * this._recordsPerPage : this._totalRecords;
    }

    public copy(pagination: Pagination) {
        Object.assign(this, pagination);
        this.recalculate();
        return this;
    }
}