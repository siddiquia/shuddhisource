import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { AgmMap, LatLngBounds } from '@agm/core';
import { google } from '@agm/core/services/google-maps-types';

@Component({
  selector: 'app-google-map',
  templateUrl: './google-map.component.html',
  styleUrls: ['./google-map.component.scss']
})
export class GoogleMapComponent implements OnInit, AfterViewInit {

  @ViewChild(AgmMap) agmMap: AgmMap;

  lat: number = 22.3152099;
  lng: number = 73.1421226;

  constructor() { }

  ngOnInit() {
  }

  ngAfterViewInit() {
    this.agmMap.mapReady.subscribe(map => {
      this.agmMap.zoom = 10;
    });

  }
}
