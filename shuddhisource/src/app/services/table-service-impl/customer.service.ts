import { Injectable } from '@angular/core';
import { TableService } from './table-service';
import { StoreService } from '../store.service';
import { Table } from 'src/app/components/table/table';
import { Customer } from 'src/app/pages/customer-page/customer';
import { Column } from 'src/app/components/table/column';
import { HttpClientWrapper } from '../../HttpClientWrapper';
import { Uriconstant } from '../../uri-constant';

@Injectable({
  providedIn: 'root'
})
export class CustomerService implements TableService {
  constructor(public store: StoreService, private httpClientWrapper: HttpClientWrapper) {
    this.getData();
  }

  getData() {
    let url = Uriconstant.END_POINT + Uriconstant.GET_CUSTOMER_LIST //+ "_currentPage=" + 1 + "&&_recordsPerPage=" + 10 + "&&_totalPages=";//"http://localhost/shuddhi/GetAllCallController/list?_currentPage=1&&_recordsPerPage=2&&_totalPages=";
    this.httpClientWrapper.post(url,getList).subscribe(response => {     
      let serviceResponse: any = response
      if (serviceResponse) {
        //let table = new Table().copy(this.store.callTable.value);
        let table = new Table().copy(this.store.customerTable.value);
        table.lazyLoad = serviceResponse.lazyLoad;//fixture.lazyLoad;
        table.data = serviceResponse.payLoad.data; //fixture.data;
       // table.lazyLoad = fixture.lazyLoad;
       // table.data = fixture.data;
        console.log("response is customer====>", table.data, "<--------");
        if (table.lazyLoad) {
          table.pagination.totalRecords = serviceResponse.totalRecords;
        } else {
          table.pagination.totalRecords = table.data.length;
        }
        table.pagination.currentPage = 1;
        table.columns = Customer.getColumns();
        this.store.customerTable.next(table);
      }
    });
  }

  changePage(page: number) {
    let table = new Table().copy(this.store.customerTable.value);
    table.pagination.currentPage = page;
    if (table.lazyLoad) {
      // service call
    } else {

    }
    this.store.customerTable.next(table);
  }

  changeRecordsPerPage(recordsPerPage: number) {
    let table = new Table().copy(this.store.customerTable.value);
    table.pagination.recordsPerPage = recordsPerPage;
    this.store.customerTable.next(table);
  }

  changeSort(col: string) {
    let table = new Table().copy(this.store.customerTable.value);
    let columns = table.columns;
    columns.filter(column => column.value !== col).forEach(column => column.sort = Column.SORT_NONE);
    columns.filter(column => column.value === col).forEach(column => {
      column.sort = ((column.sort + 2) % 3) - 1;
      table.sorting.order = column.sort;
      table.sorting.column = column.sort === Column.SORT_NONE ? null : column.value;
    });
    this.store.customerTable.next(table);
  }

  filter(col: string, query: string) {
    let table = new Table().copy(this.store.customerTable.value);
    table.columns.filter(column => column.value === col).forEach(column => column.filter = query);
    table.filtering.filters = table.columns.filter(column => column.filter && column.filter.trim().length >= 0).map(column => { return { column: column.value, value: column.filter } })
    this.store.customerTable.next(table);
  }

  updateCustomer(customerDto:any){
    
    let url = Uriconstant.END_POINT + Uriconstant.UPDATE_CUSTOMER //+ "_currentPage=" + 1 + "&&_recordsPerPage=" + 10 + "&&_totalPages=";//"http://localhost/shuddhi/GetAllCallController/list?_currentPage=1&&_recordsPerPage=2&&_totalPages=";
    this.httpClientWrapper.put(url,customerDto).subscribe(response => {  
      if (response) {
           console.log("response========================>",response);
      }
  });

}

saveCustomer(customerDto:any){
    
  let url = Uriconstant.END_POINT + Uriconstant.CREATE_CUSTOMER //+ "_currentPage=" + 1 + "&&_recordsPerPage=" + 10 + "&&_totalPages=";//"http://localhost/shuddhi/GetAllCallController/list?_currentPage=1&&_recordsPerPage=2&&_totalPages=";
  this.httpClientWrapper.post(url,customerDto).subscribe(response => {  
    if (response) {
         console.log("response========================>",response);
    }
});

}

}

const fixture = {
  lazyLoad: false,
  totalRecords: 13,
  data: [
    new Customer().copy(<Customer>{ id: 1, name: 'Manoj Shah', address: "48, Janak Nagar, Nr Jay Jalaram Nagar, Bh Mother School,", area: "Gotri Road", city: "Vadodara", pincode: "", phoneNo: "7984850253", emailId: "", description: "", active: true }),
    new Customer().copy(<Customer>{ id: 2, name: 'Ratin Bhai Mukharjee', address: "A/26, Narayan Bunglow, Nr. Punit Nagar, New Sama Road,", area: "", city: "Vadodara", pincode: "", phoneNo: "7990523892, 9375936968", emailId: "", description: "", active: true }),
    new Customer().copy(<Customer>{ id: 3, name: 'Helly Enclave (Dipak Bhai)', address: "Near Ami Society - I, Sukruti Nagar", area: "Diwalipura", city: "Vadodara", pincode: "", phoneNo: "9925029072, 9320725464, 9879654787", emailId: "", description: "Contract no 634 date 18Jan17, 4 service.", active: true })
  ]
}
export const getList={"pagination":{"from":null,"to":2},"sort":{"coulmn":null,"order":null},"filters":[{"coulmn":null,"value":null}]}