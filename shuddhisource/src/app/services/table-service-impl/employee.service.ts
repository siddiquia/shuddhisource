import { Injectable } from '@angular/core';
import { TableService } from './table-service';
import { StoreService } from '../store.service';
import { Table } from 'src/app/components/table/table';
import { Employee } from 'src/app/pages/employee-page/employee';
import { Column } from 'src/app/components/table/column';
import { Uriconstant } from '../../uri-constant';
import { HttpClientWrapper } from '../../HttpClientWrapper';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService implements TableService {

  constructor(public store: StoreService,private httpClientWrapper: HttpClientWrapper) {
    this.getData();
  }

  getData() {
    
    let url = Uriconstant.END_POINT + Uriconstant.GET_EMPLOYEE_LIST //+ "_currentPage=" + 1 + "&&_recordsPerPage=" + 10 + "&&_totalPages=";//"http://localhost/shuddhi/GetAllCallController/list?_currentPage=1&&_recordsPerPage=2&&_totalPages=";
    this.httpClientWrapper.post(url,getList).subscribe(response => {     
      let serviceResponse: any = response
      if (serviceResponse) {
    let table = new Table().copy(this.store.employeeTable.value);

    table.lazyLoad = serviceResponse.lazyLoad;
    table.data = serviceResponse.payLoad.data;

    if (table.lazyLoad) {
      table.pagination.totalRecords = serviceResponse.totalRecords;
    } else {
      table.pagination.totalRecords = table.data.length;
    }
    table.pagination.currentPage = 1;
    table.columns = Employee.getColumns();
    this.store.employeeTable.next(table);
  }
  })
  }

  changePage(page: number) {
    let table = new Table().copy(this.store.employeeTable.value);
    table.pagination.currentPage = page;
    if (table.lazyLoad) {
      // service call
    } else {

    }
    this.store.employeeTable.next(table);
  }

  changeRecordsPerPage(recordsPerPage: number) {
    let table = new Table().copy(this.store.employeeTable.value);
    table.pagination.recordsPerPage = recordsPerPage;
    this.store.employeeTable.next(table);
  }

  changeSort(col: string) {
    let table = new Table().copy(this.store.employeeTable.value);
    let columns = table.columns;
    columns.filter(column => column.value !== col).forEach(column => column.sort = Column.SORT_NONE);
    columns.filter(column => column.value === col).forEach(column => {
      column.sort = ((column.sort + 2) % 3) - 1;
      table.sorting.order = column.sort;
      table.sorting.column = column.sort === Column.SORT_NONE ? null : column.value;
    });
    this.store.employeeTable.next(table);
  }

  filter(col: string, query: string) {
    let table = new Table().copy(this.store.employeeTable.value);
    table.columns.filter(column => column.value === col).forEach(column => column.filter = query);
    table.filtering.filters = table.columns.filter(column => column.filter && column.filter.trim().length >= 0).map(column => { return { column: column.value, value: column.filter } })
    this.store.employeeTable.next(table);
  }

   saveEmployee(employeeDTO:any){
    let serviceUri =Uriconstant.END_POINT+Uriconstant.SAVE_EMPLOYEE //string, body: any
    console.log("employeeDTO-------->",employeeDTO);
     this.httpClientWrapper.post(serviceUri,employeeDTO).toPromise<any>().then(response => {
       console.log("response======>",response);
     });
   }
}

const fixture = {
  lazyLoad: false,
  totalRecords: 13,
  data: [
    new Employee().copy(<Employee>{ id: 1, userName: "nikunj", name: 'nikunj', mobileNo: "9428120749", phoneNo: "9428120749", emailId: "nikunj.ce31@gmail.com", permanentAddress: "a", active: true }),
    new Employee().copy(<Employee>{ id: 1, userName: "mi", name: 'Note', mobileNo: "4", phoneNo: "9712388899", emailId: "", permanentAddress: "", active: true }),
    new Employee().copy(<Employee>{ id: 1, userName: "nikunj", name: 'Pooja patel', mobileNo: "9825305702", phoneNo: "", emailId: "", permanentAddress: "", active: true }),
    new Employee().copy(<Employee>{ id: 1, userName: "ajay", name: 'Ajay SIngh', mobileNo: "9712388899", phoneNo: "", emailId: "", permanentAddress: "", active: true }),
    new Employee().copy(<Employee>{ id: 1, userName: "kiran", name: 'Kiran Singh', mobileNo: "9824088899", phoneNo: "", emailId: "", permanentAddress: "A / 28, Laxmikunj", active: true }),
    new Employee().copy(<Employee>{ id: 1, userName: "Saju", name: 'Saju', mobileNo: "9825011816", phoneNo: "", emailId: "", permanentAddress: "57 Payal Park, Satellite, Ahmedabad", active: true }),
    new Employee().copy(<Employee>{ id: 1, userName: "sarvesh", name: 'Sarvesh Singh', mobileNo: "9099030304", phoneNo: "", emailId: "", permanentAddress: "Ratnapark Society", active: true }),
    new Employee().copy(<Employee>{ id: 1, userName: "prakash", name: 'Prakash Dhuriya', mobileNo: "7874288388", phoneNo: "", emailId: "", permanentAddress: "", active: true }),
    new Employee().copy(<Employee>{ id: 1, userName: "suraj", name: 'Suraj Dhuriya', mobileNo: "9099044423", phoneNo: "", emailId: "shuddhi@live.in", permanentAddress: "Rachna Park", active: true })
  ]
}

// richa	Richa Singh	9712688899			A / 28, Laxmikunj	✔
// manjeet	Manjeet Saroj	7435911438				✖
// ajay2	Ajay2	8460533598				✔
// sheetla	Sheetla Prasad Saroj	7777919769			Shakti Park Soc	✔
// ajaysingh	Ajay Singh	9898705718				✔
// akmal	Akmal Siddiqui	8269231560		123@gamail.com		✔

export const getList={"pagination":{"from":null,"to":2},"sort":{"coulmn":null,"order":null},"filters":[{"coulmn":null,"value":null}]}