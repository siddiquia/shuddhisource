import { Injectable } from '@angular/core';
import { Table } from 'src/app/components/table/table';
import { StoreService } from '../store.service';
import { TableService } from './table-service';
import { Role } from 'src/app/pages/role-page/unit';
import { Column } from 'src/app/components/table/column';

@Injectable({
  providedIn: 'root'
})
export class RoleService implements TableService {

  constructor(public store: StoreService) {
    this.getData();
  }

  getData() {
    let table = new Table().copy(this.store.roleTable.value);

    table.lazyLoad = fixture.lazyLoad;
    table.data = fixture.data;

    if (table.lazyLoad) {
      table.pagination.totalRecords = fixture.totalRecords;
    } else {
      table.pagination.totalRecords = table.data.length;
    }
    table.pagination.currentPage = 1;
    table.columns = Role.getColumns();
    this.store.roleTable.next(table);
  }

  changePage(page: number) {
    let table = new Table().copy(this.store.roleTable.value);
    table.pagination.currentPage = page;
    if (table.lazyLoad) {
      // service call
    } else {

    }
    this.store.roleTable.next(table);
  }

  changeRecordsPerPage(recordsPerPage: number) {
    let table = new Table().copy(this.store.roleTable.value);
    table.pagination.recordsPerPage = recordsPerPage;
    this.store.roleTable.next(table);
  }

  changeSort(col: string) {
    let table = new Table().copy(this.store.roleTable.value);
    let columns = table.columns;
    columns.filter(column => column.value !== col).forEach(column => column.sort = Column.SORT_NONE);
    columns.filter(column => column.value === col).forEach(column => {
      column.sort = ((column.sort + 2) % 3) - 1;
      table.sorting.order = column.sort;
      table.sorting.column = column.sort === Column.SORT_NONE ? null : column.value;
    });
    this.store.roleTable.next(table);
  }

  filter(col: string, query: string) {
    let table = new Table().copy(this.store.roleTable.value);
    table.columns.filter(column => column.value === col).forEach(column => column.filter = query);
    table.filtering.filters = table.columns.filter(column => column.filter && column.filter.trim().length >= 0).map(column => { return { column: column.value, value: column.filter } })
    this.store.roleTable.next(table);
  }

}

const fixture = {
  lazyLoad: false,
  totalRecords: 13,
  data: [
    new Role().copy(<Role>{ id: 1, name: 'SUPERADMIN', description: "SuperAdmin", active: true }),
    new Role().copy(<Role>{ id: 1, name: 'Administrator', description: "Administrator", active: true }),
    new Role().copy(<Role>{ id: 1, name: 'Sales', description: "Sales", active: true }),
    new Role().copy(<Role>{ id: 1, name: 'Support', description: "Support", active: true }),
    new Role().copy(<Role>{ id: 1, name: 'BackOffice', description: "BackOffice", active: true }),
    new Role().copy(<Role>{ id: 1, name: 'ACCESS GRANT', description: "based on designation", active: true })
  ]
}