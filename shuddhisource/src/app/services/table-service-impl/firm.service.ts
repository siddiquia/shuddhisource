import { Injectable } from '@angular/core';
import { StoreService } from '../store.service';
import { Table } from '../../components/table/table';
import { Firm } from '../../pages/firm-page/firm';
import { TableService } from './table-service';
import { Column } from 'src/app/components/table/column';

@Injectable({
  providedIn: 'root'
})
export class FirmService implements TableService {

  constructor(public store: StoreService) {
    this.getData();
  }

  getData() {
    let firmTable = new Table().copy(this.store.firmTable.value);

    firmTable.lazyLoad = fixture.lazyLoad;
    firmTable.data = fixture.data;

    if (firmTable.lazyLoad) {
      firmTable.pagination.totalRecords = fixture.totalRecords;
    } else {
      firmTable.pagination.totalRecords = firmTable.data.length;
    }
    firmTable.pagination.currentPage = 1;
    firmTable.columns = Firm.getColumns();
    this.store.firmTable.next(firmTable);
  }

  changePage(page: number) {
    let firmTable = new Table().copy(this.store.firmTable.value);
    firmTable.pagination.currentPage = page;
    if (firmTable.lazyLoad) {
      // service call
    } else {

    }
    this.store.firmTable.next(firmTable);
  }

  changeRecordsPerPage(recordsPerPage: number) {
    let firmTable = new Table().copy(this.store.firmTable.value);
    firmTable.pagination.recordsPerPage = recordsPerPage;
    this.store.firmTable.next(firmTable);
  }

  changeSort(col: string) {
    let firmTable = new Table().copy(this.store.firmTable.value);
    let columns = firmTable.columns;
    columns.filter(column => column.value !== col).forEach(column => column.sort = Column.SORT_NONE);
    columns.filter(column => column.value === col).forEach(column => {
      column.sort = ((column.sort + 2) % 3) - 1;
      firmTable.sorting.order = column.sort;
      firmTable.sorting.column = column.sort === Column.SORT_NONE ? null : column.value;
    });
    this.store.firmTable.next(firmTable);
  }

  filter(col: string, query: string) {
    let firmTable = new Table().copy(this.store.firmTable.value);
    firmTable.columns.filter(column => column.value === col).forEach(column => column.filter = query);
    firmTable.filtering.filters = firmTable.columns.filter(column => column.filter && column.filter.trim().length >= 0).map(column => { return { column: column.value, value: column.filter } })
    this.store.firmTable.next(firmTable);
  }

}

const fixture = {
  lazyLoad: false,
  totalRecords: 13,
  data: Array(6).fill(
    new Firm().copy(<Firm>{ id: 1, name: 'SHUDDHI', address: 'A/28 Laxmikunj Soc. Part 3, Sama', contactPerson: 'KIRAN SINGH', mobileNo: '9824088899', phoneNo: '9712688899', emailId: 'SHUDDHI@LIVE.IN', active: true }))
    .concat(Array(7).fill(
      new Firm().copy(<Firm>{ id: 2, name: 'Shuddhi India', address: 'A', contactPerson: 'a', mobileNo: '478', phoneNo: '789', emailId: 'n@gmail.com', active: true })
    ))
}