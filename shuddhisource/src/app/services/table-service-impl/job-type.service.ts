import { Injectable } from '@angular/core';
import { TableService } from './table-service';
import { StoreService } from '../store.service';
import { Table } from 'src/app/components/table/table';
import { Column } from 'src/app/components/table/column';
import { JobType } from 'src/app/pages/job-type-page/job-type';

@Injectable({
  providedIn: 'root'
})
export class JobTypeService implements TableService {

  constructor(public store: StoreService) {
    this.getData();
  }

  getData() {
    let table = new Table().copy(this.store.jobTypeTable.value);

    table.lazyLoad = fixture.lazyLoad;
    table.data = fixture.data;

    if (table.lazyLoad) {
      table.pagination.totalRecords = fixture.totalRecords;
    } else {
      table.pagination.totalRecords = table.data.length;
    }
    table.pagination.currentPage = 1;
    table.columns = JobType.getColumns();
    this.store.jobTypeTable.next(table);
  }

  changePage(page: number) {
    let table = new Table().copy(this.store.jobTypeTable.value);
    table.pagination.currentPage = page;
    if (table.lazyLoad) {
      // service call
    } else {

    }
    this.store.jobTypeTable.next(table);
  }

  changeRecordsPerPage(recordsPerPage: number) {
    let table = new Table().copy(this.store.jobTypeTable.value);
    table.pagination.recordsPerPage = recordsPerPage;
    this.store.jobTypeTable.next(table);
  }

  changeSort(col: string) {
    let table = new Table().copy(this.store.jobTypeTable.value);
    let columns = table.columns;
    columns.filter(column => column.value !== col).forEach(column => column.sort = Column.SORT_NONE);
    columns.filter(column => column.value === col).forEach(column => {
      column.sort = ((column.sort + 2) % 3) - 1;
      table.sorting.order = column.sort;
      table.sorting.column = column.sort === Column.SORT_NONE ? null : column.value;
    });
    this.store.jobTypeTable.next(table);
  }

  filter(col: string, query: string) {
    let table = new Table().copy(this.store.jobTypeTable.value);
    table.columns.filter(column => column.value === col).forEach(column => column.filter = query);
    table.filtering.filters = table.columns.filter(column => column.filter && column.filter.trim().length >= 0).map(column => { return { column: column.value, value: column.filter } })
    this.store.jobTypeTable.next(table);
  }

}

const fixture = {
  lazyLoad: false,
  totalRecords: 13,
  data: [
    new JobType().copy(<JobType>{ id: 1, name: "Sofa Shampooing", unitName: "123", description: "", active: true }),
    new JobType().copy(<JobType>{ id: 1, name: "House Cleaning", unitName: "123", description: "1", active: true }),
    new JobType().copy(<JobType>{ id: 1, name: "Sofa Steam Wash", unitName: "123", description: "Sofa Steam Wash", active: true }),
    new JobType().copy(<JobType>{ id: 1, name: "Carpet Shampooing", unitName: "123", description: "", active: true }),
    new JobType().copy(<JobType>{ id: 1, name: "Carpet Steam Wash", unitName: "123", description: "", active: true }),
    new JobType().copy(<JobType>{ id: 1, name: "Glass Cleaning", unitName: "123", description: "", active: true }),
    new JobType().copy(<JobType>{ id: 1, name: "Chair Steam Wash", unitName: "123", description: "", active: true }),
    new JobType().copy(<JobType>{ id: 1, name: "Chair Shampooing", unitName: "123", description: "", active: true }),
    new JobType().copy(<JobType>{ id: 1, name: "Complaint", unitName: "123", description: "", active: true }),
    new JobType().copy(<JobType>{ id: 1, name: "Site Visit", unitName: "123", description: "", active: true }),
    new JobType().copy(<JobType>{ id: 1, name: "Office Cleaning", unitName: "123", description: "", active: true }),
    new JobType().copy(<JobType>{ id: 1, name: "Car Interior Cleaning", unitName: "123", description: "", active: true }),
    new JobType().copy(<JobType>{ id: 1, name: "Sofa Dusting", unitName: "123", description: "", active: true }),
    new JobType().copy(<JobType>{ id: 1, name: "WATER TANK CLEANING", unitName: "123", description: "", active: true })
  ]
}
