import { Injectable } from '@angular/core';
import { TableService } from './table-service';
import { StoreService } from '../store.service';
import { Table } from 'src/app/components/table/table';
import { Unit } from 'src/app/pages/unit-page/unit';
import { Column } from 'src/app/components/table/column';

@Injectable({
  providedIn: 'root'
})
export class UnitService implements TableService {

  constructor(public store: StoreService) {
    this.getData();
  }

  getData() {
    let table = new Table().copy(this.store.unitTable.value);

    table.lazyLoad = fixture.lazyLoad;
    table.data = fixture.data;

    if (table.lazyLoad) {
      table.pagination.totalRecords = fixture.totalRecords;
    } else {
      table.pagination.totalRecords = table.data.length;
    }
    table.pagination.currentPage = 1;
    table.columns = Unit.getColumns();
    this.store.unitTable.next(table);
  }

  changePage(page: number) {
    let table = new Table().copy(this.store.unitTable.value);
    table.pagination.currentPage = page;
    if (table.lazyLoad) {
      // service call
    } else {

    }
    this.store.unitTable.next(table);
  }

  changeRecordsPerPage(recordsPerPage: number) {
    let table = new Table().copy(this.store.unitTable.value);
    table.pagination.recordsPerPage = recordsPerPage;
    this.store.unitTable.next(table);
  }

  changeSort(col: string) {
    let table = new Table().copy(this.store.unitTable.value);
    let columns = table.columns;
    columns.filter(column => column.value !== col).forEach(column => column.sort = Column.SORT_NONE);
    columns.filter(column => column.value === col).forEach(column => {
      column.sort = ((column.sort + 2) % 3) - 1;
      table.sorting.order = column.sort;
      table.sorting.column = column.sort === Column.SORT_NONE ? null : column.value;
    });
    this.store.unitTable.next(table);
  }

  filter(col: string, query: string) {
    let table = new Table().copy(this.store.unitTable.value);
    table.columns.filter(column => column.value === col).forEach(column => column.filter = query);
    table.filtering.filters = table.columns.filter(column => column.filter && column.filter.trim().length >= 0).map(column => { return { column: column.value, value: column.filter } })
    this.store.unitTable.next(table);
  }

}

const fixture = {
  lazyLoad: false,
  totalRecords: 13,
  data: Array(6).fill(
    new Unit().copy(<Unit>{ id: 1, name: '123', description: "desc goes here", active: true }))
    .concat(Array(7).fill(
      new Unit().copy(<Unit>{ id: 2, name: '456', active: false })
    ))
}