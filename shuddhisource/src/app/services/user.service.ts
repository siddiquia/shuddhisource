import { Injectable } from '@angular/core';
import { StoreService, User } from './store.service';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(
    public store: StoreService
  ) {
    this.getUser();
  }

  getUser() {
    let user = new User().copy(this.store.user.value);
    user.name = "Srajan Pathak";
    user.status = true;
    user.login = true;
    this.store.user.next(user);
  }
}
