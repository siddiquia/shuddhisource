import { Component, OnInit } from '@angular/core';
import { StoreService, User } from 'src/app/services/store.service';
import { SideMenuService } from 'src/app/services/side-menu.service';
import { SideMenu } from './side-menu';

@Component({
  selector: 'app-side-menu',
  templateUrl: './side-menu.component.html',
  styleUrls: ['./side-menu.component.scss']
})
export class SideMenuComponent implements OnInit {

  public sideMenu: SideMenu;
  public user: User;

  constructor(
    public store: StoreService,
    public sideMenuService: SideMenuService
  ) {
    this.store.sideMenu.subscribe(sideMenu => {
      this.sideMenu = sideMenu;
    });
    this.store.user.subscribe(user => {
      this.user = user;
    });
  }

  ngOnInit() {
  }

  toggleSubMenu(event: MouseEvent, index: number) {
    event.stopPropagation();
    this.sideMenuService.toggleSubMenu(index);
  }

  menuItemClick(index: number) {
    this.sideMenuService.menuItemClick(index);
  }
  subMenuClick(event: MouseEvent, i: number, j: number) {
    event.stopPropagation();
    this.sideMenuService.subMenuClick(i, j);
  }
}
