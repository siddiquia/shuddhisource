

export class MenuItem {
    public icon: string;
    public text: string;
    public subMenu: MenuItem[];
    public dropdown: boolean;
    public active: boolean;
    public link: string;

    constructor() {
        this.text = "menu item";
        this.dropdown = false;
        this.active = false;
        this.subMenu = [];
        this.link = "/";
    }

    public copy(menuItem: MenuItem): MenuItem {
        return Object.assign(this, menuItem);
    }

}