

export class Filtering {
    public filters: { column: string, value: string }[];

    constructor() {
        this.filters = [];
    }

    public copy(filtering: Filtering) {
        return Object.assign(this, filtering);
    }
}