import { Component, OnInit, Input, Output,EventEmitter } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Table } from './table';
import { Firm } from 'src/app/pages/firm-page/firm';
import { Pagination } from '../pagination/pagination';
import { Sorting } from './sorting';
import { Filtering } from './filtering';
import { TableService } from 'src/app/services/table-service-impl/table-service';
import { Column } from "./column"; 
import { NavigationService } from '../../services/navigation.service'; 

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit {

  @Input('table') tableData: BehaviorSubject<Table>;
  @Input('service') service: TableService;
  @Input() pageName:string;
  @Input() content:any 
  closeResult: string; 
  @Output() selectedId= new EventEmitter<string>();
  @Input() customerSelectedId:any;

  column = Column;

  dataList: Firm[] = [];
  columns: Column[] = [];
  pagination: Pagination;
  sorting: Sorting;
  filtering: Filtering; 

  constructor(public navigation: NavigationService) { }

  ngOnInit() {
    this.tableData.subscribe(table => {
      this.dataList = <Firm[]>table.displayData;
      this.columns = table.columns;
      this.pagination = table.pagination;
      this.sorting = table.sorting;
      this.filtering = table.filtering;
    })
  }

  filter(col: string, event: Event) {
    this.service.filter(col, (<HTMLInputElement>event.target).value);
  }

  sort(col: string) {
    this.service.changeSort(col);
  }

  edit(event: MouseEvent, id: number) {
    if(this.pageName == "Customer Master"){ 
      this.customerSelectedId=id;
      this.selectedId.emit(this.customerSelectedId); 
      this.navigation.navigateTo([this.navigation.router.url+'/add']); 
      
    }else if(this.pageName == "Call Master"){ 
      this.customerSelectedId=id;
      this.selectedId.emit(this.customerSelectedId); 
      this.navigation.navigateTo([this.navigation.router.url+'/add']); 
    }else if(this.pageName == "Customer Master"){ 
    }
    event.preventDefault();
  }
 /*  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  } */
}
