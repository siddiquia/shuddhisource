import { Component, OnInit } from '@angular/core';
import { CallBox } from './call-box';
import { StoreService } from 'src/app/services/store.service';
import { CallBoxService } from 'src/app/services/call-box.service';

@Component({
  selector: 'app-call-box',
  templateUrl: './call-box.component.html',
  styleUrls: ['./call-box.component.scss']
})
export class CallBoxComponent implements OnInit {

  public callBox: CallBox;

  constructor(
    public store: StoreService,
    public callBoxService: CallBoxService
  ) {
    this.store.callBox.subscribe(callBox => {
      this.callBox = callBox;
    });
  }

  ngOnInit() {
  }

}
