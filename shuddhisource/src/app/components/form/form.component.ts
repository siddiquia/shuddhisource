import { Component, OnInit, Input, Output, EventEmitter, OnChanges } from '@angular/core';
import { Form, FormControlInput, FormControl, FormControlCheckbox, FormControlDropdown, FormControlTextarea, FormControlCheckboxGroup } from './form';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit, OnChanges {

  @Input()
  form: Form = new Form();

  @Output("formSubmit") submit: EventEmitter<{ name: string, value: any }[]> = new EventEmitter<{ name: string, value: any }[]>();

  constructor() {
  }

  ngOnInit() {
  }
  ngOnChanges(){
    console.log("form=======>",this.form)
  }

  formSubmit() {
    let temp = this.form.inputs.filter(input => input.control === FormControl.CONTROL_CHECKBOX_GROUP).map(input => (<FormControlCheckboxGroup>input).checkboxes).reduce((a, b) => { return a.concat(b) }, []).concat(this.form.inputs.filter(input => input.control !== FormControl.CONTROL_CHECKBOX_GROUP));
    let formData = temp.map(input => { return { name: input.name, value: input.value } }).reduce((a, b) => {
      let index = a.findIndex(obj => obj.name === b.name);
      if (index >= 0) {
        a[index] = b;
      } else {
        a.push(b);
      }
      return a
    }, []);
    this.submit.emit(formData);
  }
}
