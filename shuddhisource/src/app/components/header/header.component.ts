import { Component, OnInit } from '@angular/core';
import { StoreService, User } from 'src/app/services/store.service';
import { SideMenuService } from 'src/app/services/side-menu.service';
import { Header } from './header';
import { SideMenu } from '../side-menu/side-menu';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  public header: Header;
  public user: User;
  public sideMenu: SideMenu;

  constructor(
    public store: StoreService,
    public sideMenuService: SideMenuService
  ) {
    this.store.header.subscribe(header => {
      this.header = header;
    });
    this.store.user.subscribe(user => {
      this.user = user;
    });

    this.store.sideMenu.subscribe(sideMenu => {
      this.sideMenu = sideMenu;
    })
  }

  ngOnInit() {
  }

  toggleMenu() {
    this.sideMenuService.toggleSideMenu();
  }

}
