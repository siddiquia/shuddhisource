import { Component, OnInit, Input, OnChanges, Output, EventEmitter } from '@angular/core';
import { CustomerService } from 'src/app/services/table-service-impl/customer.service';
import { StoreService } from 'src/app/services/store.service';
import { NavigationService } from 'src/app/services/navigation.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-customer-page',
  templateUrl: './customer-page.component.html',
  styleUrls: ['./customer-page.component.scss']
})
export class CustomerPageComponent implements OnInit {
  closeResult: string;
  registerForm: FormGroup;
  submitted = false;
  pageName: string = 'Customer Master';
  @Input()rowSelecteId:string
  @Input() customerSelectedId;
  @Input() content;
  @Output() selectedIDForEdit= new EventEmitter<string>();
  constructor(
    public store: StoreService,
    public service: CustomerService,
    public navigation: NavigationService,
    // public modalService: NgbModal,
    //public formBuilder: FormBuilder
  ) { }

  ngOnInit() {
    /*  this.registerForm = this.formBuilder.group({
       fullName: ['', Validators.required],
       address: ['', Validators.required],
       address2: ['', Validators.required],
       address3: ['', Validators.required],
       area: ['', Validators.required],
       city: ['', Validators.required],
       pinCode: ['', Validators.required],
       phoneNo: ['', Validators.required],
       phoneNo2: ['', Validators.required],
       phoneNo3: ['', Validators.required],
       email: ['', [Validators.required, Validators.email]],
       desc: ['', Validators.required],
   }); */
  }
  
  // convenience getter for easy access to form fields
  get f() { return this.registerForm.controls; }
  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.registerForm.invalid) {
      return;
    }

    alert('SUCCESS!! :-)');
  }
  /*  open(content) {
     this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title', size: 'lg'}).result.then((result) => {
       this.closeResult = `Closed with: ${result}`;
     }, (reason) => {
       this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
     });
   } */

  /*  private getDismissReason(reason: any): string {
     if (reason === ModalDismissReasons.ESC) {
       return 'by pressing ESC';
     } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
       return 'by clicking on a backdrop';
     } else {
       return  `with: ${reason}`;
     }
   } */
   getSelectedId(event){
console.log("test=========>",event);
//this.rowSelecteId=event
this.selectedIDForEdit.emit(event)
   }

}
