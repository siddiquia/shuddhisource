import { Component, OnInit } from '@angular/core';
import { StoreService } from 'src/app/services/store.service';
import { UnitService } from 'src/app/services/table-service-impl/unit.service';
import { NavigationService } from 'src/app/services/navigation.service';

@Component({
  selector: 'app-unit-page',
  templateUrl: './unit-page.component.html',
  styleUrls: ['./unit-page.component.scss'],
})
export class UnitPageComponent implements OnInit {

  constructor(
    public store: StoreService,
    public service: UnitService,
    public navigation: NavigationService
  ) { }

  ngOnInit() {
  }

}
