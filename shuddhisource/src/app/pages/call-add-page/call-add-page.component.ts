import { Component, OnInit } from '@angular/core';
import { Form, FormControlInput, FormControlTextarea, FormControlCheckbox, FormControlDropdown } from 'src/app/components/form/form';
import { NavigationService } from 'src/app/services/navigation.service';
import { CallService } from '../../services/table-service-impl/call.service';
import { HttpClientWrapper } from '../../HttpClientWrapper';
import { Uriconstant } from '../../uri-constant';

@Component({
  selector: 'app-call-add-page',
  templateUrl: './call-add-page.component.html',
  styleUrls: ['./call-add-page.component.scss']
})
export class CallAddPageComponent implements OnInit {

  public form: Form;

  constructor(public navigation: NavigationService,private callService:CallService,public httpClientWrapper: HttpClientWrapper) {

    let firmName = new FormControlDropdown();
    firmName.name = "firmName";
    firmName.label = "FirmName";
    //firmName.placeholder = "Unit Name";
    firmName.options = [
      { value: "1", displayValue: "Shuddhi India" },
      { value: "2", displayValue: "Shuddhi" }, 
    ]

    let CustomerName = new FormControlDropdown();
    CustomerName.name = "customerName";
    CustomerName.label = "CustomerName";
    //firmName.placeholder = "Unit Name";
    CustomerName.options = [
      { value: "1", displayValue: "Ajay" },
      { value: "2", displayValue: "Akram" },
      { value: "3", displayValue: "Rajendra" },
      { value: "4", displayValue: "Pushpendra" },
      { value: "5", displayValue: "Ric" },
      { value: "6", displayValue: "MA" },
    ]

    let jobTypeName = new FormControlDropdown();
    jobTypeName.name = "jobTypeName";
    jobTypeName.label = "JobType Name";
    //firmName.placeholder = "Unit Name";
    jobTypeName.options = [
      { value: "1", displayValue: "Sofa" },
      { value: "2", displayValue: "House Cleaning" },
      { value: "3", displayValue: "Sofa Steam Wash" }, 
    ]

    let employeeName = new FormControlDropdown();
    employeeName.name = "employeeName";
    employeeName.label = "Employee Name";
    //firmName.placeholder = "Unit Name";
    employeeName.options = [
      { value: "1", displayValue: "A1" },
      { value: "2", displayValue: "B1" },
      { value: "3", displayValue: "C1" }, 
    ]

    let customerPreferredTime = new FormControlInput();
    customerPreferredTime.placeholder = "customerPreferredTime";
    customerPreferredTime.label = "CustomerPreferredTime";
    customerPreferredTime.name = "dustomerPreferredTime";
    customerPreferredTime.type = "date"; 

    let description = new FormControlTextarea();
    description.name = "description";
    description.placeholder = "Description";
    description.label = "Description";
    description.rows = 4;

    let amount = new FormControlInput();
    amount.placeholder = "Amount";
    amount.label = "Amount";
    amount.name = "Amount";
    amount.type = "text";
     
    this.form = new Form();
    this.form.name = "firm";
    this.form.submit = "Save";
    this.form.cancel = null;
    this.form.inputs = [
      firmName, CustomerName, jobTypeName, employeeName, customerPreferredTime, description, amount
    ]

  }

  ngOnInit() {
    let url = Uriconstant.END_POINT + Uriconstant.GET_CALL_BY_ID
    this.httpClientWrapper.get(url, { id: 8 }).subscribe(response => {
      if (response) {
        this.form.inputs.forEach(obj => { 
          if(obj.name=='firmId'){
               obj.value=response["payLoad"].data[0].firmId
          }else if(obj.name=='customerId'){
            obj.value=response["payLoad"].data[0].customerId
          }else if(obj.name=='description'){
            obj.value=response["payLoad"].data[0].description
          }else if(obj.name=='jobType'){
            obj.value=response["payLoad"].data[0].jobType
          }else if(obj.name=='preferredTime'){
            obj.value=response["payLoad"].data[0].preferredTime
          } 
      
      });
  }
    })
  }
  onSubmit(formData) {
    console.log(formData);
  
    if (formData) {
      console.log("formData response s", formData);
      formData.forEach(obj => {
         if(obj.name=='firmName'){
          console.log("obj---123->",obj.value)
          callDTO.firmId=obj.value;
        }else if(obj.name=='callId'){
          callDTO.callId=obj.value
        }else if(obj.name=='customerId'){
          callDTO.customerId=obj.value
        }else if(obj.name=='employeeId'){
          callDTO.employeeId=obj.value
        }else if(obj.name=='customerprefferedTime'){
          callDTO.customerprefferedTime=obj.value
        }else if(obj.name=='description'){
          callDTO.description=obj.value
        }else if(obj.name=='amount'){
          callDTO.amount=obj.value
        } 
      })
  }
  if(callDTO.id==8){
    callDTO.id=8
    this.callService.updateCustomer(callDTO);
  }else{
    callDTO.id=0;
    this.callService.saveCustomer(callDTO);
  }
}
}

  export const callDTO={  
    "id":0,
    "firmId":"1",
    "callId":1,
    "customerId":1,
    "jobTypeId":"11",
    "employeeId":"1",
    "customerprefferedTime":"1544891083000",
    "description":"test",
    "amount":1200
    
  }


