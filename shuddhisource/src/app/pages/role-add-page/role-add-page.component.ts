import { Component, OnInit } from '@angular/core';
import { Form, FormControlInput, FormControlTextarea, FormControlCheckbox } from 'src/app/components/form/form';
import { NavigationService } from 'src/app/services/navigation.service';

@Component({
  selector: 'app-role-add-page',
  templateUrl: './role-add-page.component.html',
  styleUrls: ['./role-add-page.component.scss']
})
export class RoleAddPageComponent implements OnInit {

  public form: Form;

  constructor(public navigation: NavigationService) {

    let name = new FormControlInput();
    name.placeholder = "Name";
    name.label = "Name";
    name.name = "name";
    name.type = "text";
    name.required = true;

    let description = new FormControlTextarea();
    description.name = "description";
    description.placeholder = "Description";
    description.label = "Description";
    description.rows = 4;

    let active = new FormControlCheckbox();
    active.label = "Active";
    active.name = "active";


    this.form = new Form();
    this.form.name = "firm";
    this.form.submit = "Save";
    this.form.cancel = null;
    this.form.inputs = [
      name, description, active
    ]

  }

  ngOnInit() {
  }

  onSubmit(formData) {
    console.log(formData);
  }

}
