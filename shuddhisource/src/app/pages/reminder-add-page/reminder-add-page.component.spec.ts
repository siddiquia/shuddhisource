import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReminderAddPageComponent } from './reminder-add-page.component';

describe('ReminderAddPageComponent', () => {
  let component: ReminderAddPageComponent;
  let fixture: ComponentFixture<ReminderAddPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReminderAddPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReminderAddPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
