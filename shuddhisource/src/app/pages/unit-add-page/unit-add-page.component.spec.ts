import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UnitAddPageComponent } from './unit-add-page.component';

describe('UnitAddPageComponent', () => {
  let component: UnitAddPageComponent;
  let fixture: ComponentFixture<UnitAddPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UnitAddPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UnitAddPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
