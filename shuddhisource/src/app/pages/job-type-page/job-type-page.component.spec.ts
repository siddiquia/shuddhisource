import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JobTypePageComponent } from './job-type-page.component';

describe('JobTypePageComponent', () => {
  let component: JobTypePageComponent;
  let fixture: ComponentFixture<JobTypePageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JobTypePageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JobTypePageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
