import { Component, OnInit } from '@angular/core';
import { Form, FormControlInput, FormControlTextarea, FormControlCheckbox, FormControlDropdown } from 'src/app/components/form/form';
import { NavigationService } from 'src/app/services/navigation.service';

@Component({
  selector: 'app-job-type-add-page',
  templateUrl: './job-type-add-page.component.html',
  styleUrls: ['./job-type-add-page.component.scss']
})
export class JobTypeAddPageComponent implements OnInit {

  public form: Form;

  constructor(public navigation: NavigationService) {

    let name = new FormControlInput();
    name.placeholder = "Name";
    name.label = "Name";
    name.name = "name";
    name.type = "text";
    name.required = true;

    let unitName = new FormControlDropdown();
    unitName.name = "unitName";
    unitName.label = "Unit Name";
    unitName.placeholder = "Unit Name";
    unitName.options = [
      { value: "123", displayValue: "unit1" },
      { value: "234", displayValue: "unit2" },
      { value: "345", displayValue: "unit3" }
    ]

    let description = new FormControlTextarea();
    description.name = "description";
    description.placeholder = "Description";
    description.label = "Description";
    description.rows = 4;

    let active = new FormControlCheckbox();
    active.label = "Active";
    active.name = "active";


    this.form = new Form();
    this.form.name = "firm";
    this.form.submit = "Save";
    this.form.cancel = null;
    this.form.inputs = [
      name, unitName, description, active
    ]

  }

  ngOnInit() {
  }

  onSubmit(formData) {
    console.log(formData);
  }

}
