import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JobTypeAddPageComponent } from './job-type-add-page.component';

describe('JobTypeAddPageComponent', () => {
  let component: JobTypeAddPageComponent;
  let fixture: ComponentFixture<JobTypeAddPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JobTypeAddPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JobTypeAddPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
