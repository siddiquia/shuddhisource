import { Component } from '@angular/core';
import { StoreService, User } from './services/store.service';
import { UserService } from './services/user.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  menuOpen: boolean;
  user: User;

  constructor(
    public store: StoreService,
    public userService: UserService
  ) {
    this.store.sideMenu.subscribe(sideMenu => {
      this.menuOpen = sideMenu.menuOpen;
    });
    this.store.user.subscribe(user => {
      this.user = user;
    });
  }

}
